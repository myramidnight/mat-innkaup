This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
## Getting Started

### Prerequisites ( what do you need first? )

Before you get started, you will need to have certain things installed on your system.

* Code editor of your choice (t.d. Visual Studio Code) 
* [Yarn](https://yarnpkg.com/en/) because it manages the packages.
* [Node.js](https://nodejs.org/en/download/) version `10.x.x` 
    * yarn seems offer to install node alongside itself if missing
* [SASS](https://sass-lang.com/install) if you are doing `CSS` in the project 

> easy way to see if you have something installed is to simply write the base command and press enter and if you want the version then just add `--version` or `-v` after it.
> if you get "command not found" then you simply don't have it installed (or wrote it incorrectly?)
> * Examples: `node --version`, `yarn --version` and `sass --version`

These are basic tools for programming, so you might want to simply install them globally. 
I am not going to provide instructions for installing these because they each provide their own documentation.

### Clone the project
>>>
Clone the project to your computer, big blue button in the top right corner. 
There are two options for cloning the project. 
* Clone with SSH
    * requires you to have or generate a SSH key and save it in your account settings. 
    * They provide good documentation on how to generate a new SSH key that you can use, done through the terminal
    * Using the SSH key is authentication, so you do not need to manually authenticate yourself
* Clone with HTTPS 
    * requires you to use your gitlab username and password, 
    * if you logged in with another account such as github or google, then you need to go into your account settings and create a password
    * If you happen to write the wrong password/username and don't get a new prompt for it, then you might need to go through your computer controle panel and delete your stored login manually
>>>
### Add the `config` file 
>>>
In order to protect sensitive details from the public,
the config details have been moved to a separate file that is ignored by git 
(meaning that it will not be added to the repository). 
Because of this, you are required to create this config file yourself. 

1. Create a file named `config.js` in the `src/lib/constants` folder 
2. then export the config objects from within it. 

Currently it only contains the Firebase config settings, file should look like this:
```javascript
export const firebase = {
  apiKey: "api-key",
  authDomain: "project-id.firebaseapp.com",
  databaseURL: "https://project-id.firebaseio.com",
  projectId: "project-id",
  storageBucket: "project-id.appspot.com",
  messagingSenderId: "sender-id",
}
```
>>>
### Install dependencies
>>>
Before you can start working on the project, you will need to insstall all the dependencies needed. 
Open the terminal in your project directory and run the following line

```
yarn
```
Tip:
    in VScode you can press `AltGr` + `Æ` to open the terminal within the editor (on Icelandic keyboard)
>>>

## Development
>>>
### `yarn start` (start the dev server)

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### [Read our Guidelines for contributing](/info/README-contributing.md) 
* [Git notes/tips](/info/README-git.md) 
* [SASS notes and guidelines](/info/README-sass.md) 

### Remember to work on a separate branch ([ view the Git notes/tips](/info/README-git.md) ) and not directly on the  `master` branch.
>>>

## Deployment
>>>
We will be using the CI/CD on GitLab to automatically build and deploy the master branch to pages for public viewing.
* [https://myramidnight87.gitlab.io/mat-innkaup/](https://myramidnight87.gitlab.io/mat-innkaup/)
>>>

## Project Built With

* [create-react-app](https://facebook.github.io/create-react-app/docs/getting-started) (framework)
* [react-router-dom](https://reacttraining.com/react-router/web/guides/quick-start)
* [react-redux](https://redux.js.org/basics/usage-with-react)
* [Yarn](https://yarnpkg.com/en/) (dependency managment)
* [SASS](https://sass-lang.com/install) (CSS extension)
