import React from 'react'
import ReactDOM from 'react-dom'
import {  HashRouter as Router } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import reducer from './reducer'
//components
import App from './App'
//ServiceWorker
import * as serviceWorker from './serviceWorker'
//stylesheet
import './scss/styles.scss'
//The redux storage
const store = createStore( reducer );

/* ---------------------------------------------------
  Render the app
---------------------------------------------------- */
/* render the base app */
ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
