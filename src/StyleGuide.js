import React from 'react'
import icons from './lib/constants/icons'
import SVG from './screens/components/SVG'

import Swipe from 'react-easy-swipe';


class StyleGuide extends React.Component {
  _swipeLeft = (direction)=>{
    console.log("You swiped me left!")
  }
  _swipeRight = (direction)=>{
    console.log("You swiped me right!")
  }
  componentWillMount(){

  }
  render(){
    const icon = icons.icon
    return(
      <div>
        <Swipe 
          onSwipeLeft={this._swipeLeft}
          onSwipeRight={this._swipeRight}
        >
          <h1>StyleGuide</h1>
          <img id="back_icon" src={icon.back} alt="back"/>
          <img src={icon.check} alt=""/>
          <img src={icon.coin} alt=""/>
          <img src={icon.delivery} alt=""/>
          <img src={icon.edit} alt=""/>
          <img src={icon.exit} alt=""/>
          <img src={icon.forward} alt=""/>
          <img src={icon.list} alt=""/>
          <img src={icon.menu} alt=""/>
          <img src={icon.piggy} alt=""/>
          <img src={icon.gear} alt=""/>
          <img src={icon.add} alt=""/>
          <img src={icon.starFill} alt=""/>
          <img src={icon.star} alt=""/>
          <img src={icon.trash} alt=""/>
          <img src={icon.userFill} alt=""/>
          <img src={icon.checkbox} alt=""/>
          <img src={icon.checkboxChecked} alt=""/>
          <br />
          <SVG className="testing_svg" data={icons.svg.exit}  height="500" width="500"/>
          <div className="testing_svg_parent">
            <SVG data={icons.svg.exit}/>
            <SVG data={icons.svg.piggy} fill="green" height="20px" width="20px" />
            <SVG className="testing_svg" data={icons.svg.exit} fill="yellow"/>
            <SVG data={icons.svg.piggy} fill="green" />
            <SVG data={icons.svg.piggy} fill="green" />
          </div>
        </Swipe>
      </div>
    )
  }
}

export default StyleGuide