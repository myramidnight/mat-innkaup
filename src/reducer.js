const defaultState = {
  userGroups : [] ,
  quicklists : [],
  userInfo : {},
  currentGroup: {items:[],access:[]},
  isLoggedIn: false,
  language: 'is',
  toggle: false,
  toggleNav: false,
  loading: true,
  screenTransition: 'fade',
  currentScreen: 'GROUP'
}

//The actionTypes
const actionTypes = {
  USER_GROUPS : 'USER_GROUPS', //get shopping lists
  CURRENT_GROUP : 'CURRENT_GROUP', //this might be removed, 'USER_GROUPS' covers this.
  UPDATE_USER_INFO : 'UPDATE_USER_INFO', //get user info
  IS_LOGGED_IN : 'IS_LOGGED_IN',
  UPDATE_LANG: 'UPDATE_LANG',
  TOGGLE_MODAL: 'TOGGLE_MODAL',
  TOGGLE_NAV: 'TOGGLE_NAV',
  SET_LOADING: 'SET_LOADING',
  UPDATE_QUICKLISTS: 'UPDATE_QUICKLISTS',
  SET_SCREEN_TRANSITION: 'SET_SCREEN_TRANSITION',
  UPDATE_CURRENT_SCREEN: 'UPDATE_CURRENT_SCREEN'
}

const reducer = (state=defaultState, action)=>{
  //console.log("Redux action: ", action.type)
  switch( action.type ) {
    case 'IS_LOGGED_IN':
      return { ...state, isLoggedIn: action.payload}
    case 'USER_GROUPS':
      return { ...state, userGroups: action.payload }
    case 'CURRENT_GROUP':
      return { ...state, currentGroup: action.payload }
    case 'UPDATE_USER_INFO':
      return { ...state, userInfo: action.payload }
    case 'UPDATE_LANG':
      return { ...state, language: action.payload }
    case 'TOGGLE_MODAL':
      return { ...state, toggle: action.payload }
    case 'TOGGLE_NAV':
      return { ...state, toggleNav: action.payload}
    case 'SET_LOADING':
      return { ...state, loading: action.payload}
    case 'UPDATE_QUICKLISTS':
      return { ...state, quicklists: action.payload}
    case 'SET_SCREEN_TRANSITION':
      return { ...state, screenTransition: action.payload}
    case 'UPDATE_CURRENT_SCREEN':
      return { ...state, currentScreen: action.payload}
    default:
      return state
  }
}

//Export the actions
export const userGroups = ( groups )=>({ type: actionTypes.USER_GROUPS, payload:groups })
export const currentGroup = ( data )=>({ type: actionTypes.CURRENT_GROUP, payload: data })
export const updateUserInfo = ( data )=>({ type: actionTypes.UPDATE_USER_INFO, payload: data })
export const isLoggedIn = ( status )=>({ type: actionTypes.IS_LOGGED_IN, payload: status })
export const updateLang = ( language )=>({ type: actionTypes.UPDATE_LANG, payload: language})
export const toggleModal = (state)=>({ type: actionTypes.TOGGLE_MODAL, payload: state})
export const toggleNav = (state)=>({type: actionTypes.TOGGLE_NAV, payload: state})
export const setLoading = (truefalse)=>({type: actionTypes.SET_LOADING, payload: truefalse})
export const updateQuicklists = (lists) =>({type: actionTypes.UPDATE_QUICKLISTS, payload: lists})
export const setScreenTransition = (transitionClass) =>({type: actionTypes.SET_SCREEN_TRANSITION, payload: transitionClass})
export const updateCurrentScreen = (screen) => ({type:actionTypes.UPDATE_CURRENT_SCREEN, payload: screen})

//Export the actions together
export const actions = {
  userGroups: userGroups,
  currentGroup: currentGroup,
  updateUserInfo: updateUserInfo,
  isLoggedIn: isLoggedIn,
  updateLang: updateLang,
  toggleModal: toggleModal,
  toggleNav: toggleNav,
  setLoading: setLoading,
  updateQuicklists: updateQuicklists,
  setScreenTransition: setScreenTransition,
  updateCurrentScreen: updateCurrentScreen,
}

//export the reducer by default
export default reducer;
