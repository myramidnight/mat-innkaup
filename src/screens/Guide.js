import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
//import { actions } from '../reducer'
//Components
import Header from './components/Header'
import Footer from './components/Footer'

class Guide extends React.Component {
  render(){
    return(
      <div className="wrapper">
        <Header title="Sýnikennsla"/>
        <Footer />
      </div>
    )
  }
}

export default withRouter(connect(null)(Guide))