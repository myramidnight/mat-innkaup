import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { actions } from '../reducer'
//Local modules
import database from '../lib/modules/database'
//Constants
import PAGES from '../lib/constants/pages'
//Components
import Header from './components/Header'
import Footer from './components/Footer'
import ItemsList from './components/ItemsList'
import CreateNewItem from './components/CreateNewItem'
import AddQuicklist from './subScreens/AddQuicklist'
import DesktopNav from './components/DesktopNav'
import QuicklistsIndex from './subScreens/QuicklistsIndex';

class SingleQuicklist extends React.Component {
  //local state
  constructor(props){
    super(props)
    this.state = {
      settingsToggle: false,
      selectGroupToggle: false,
      headerAction: {action:this._addItemToGroup, svg:"add"}, //default header action
      actionAdd: {action:this._addItemToGroup, svg:"add"},
      actionClose: {action:this._closeOverlay, svg: "exit"},
      settingsAction: {action:this._editSettings, svg:"edit", text:this.props.TEXT.EDIT}
    }
  }
  /* ---------------------------------------------------
    local components
  ---------------------------------------------------- */
  ItemsList = ()=>{
    if(this.state.settingsToggle) {
      return(
        <div>
          <ItemsList 
            action={{
              action:this._removeItem, //removes the item from database
              svg: "exit"}}
            updateRedux={this._updateRedux}
            quicklist
          />
          <CreateNewItem action={this._addItem} />
          <button className="delete-list" onClick={()=>{ this._deleteList()}}>{this.props.TEXT.DELETE_LIST}</button>
        </div>
      )
    } else {
      return(
        <ItemsList 
          updateRedux={this._updateRedux}
          action={{
            action:this._addItemToGroup, //opens the overlay for selecting group
            svg: "add"}}
          quicklist
        />
      )
    }
  }
  QuicklistOverlays = ()=>{
    if(this.state.selectGroupToggle){
      return <AddQuicklist /> //its like the groupSettings for quicklists
    } else {
      return <this.ItemsList />
    }
  }
  
  /* ---------------------------------------------------
    component functions
  ---------------------------------------------------- */
  _closeOverlay = ()=>{
    //closes any overlay and changes the headerAction back into adding list to groups
    //need to change this to only work for addListToGroup, the settings toggle should just change to "vista"
    this.setState({selectGroupToggle:false, settingsToggle: false, headerAction:this.state.actionAdd})
  }
  _updateRedux = ()=>{
    const self = this
    //update the userGroups array with currentGroup changes
    const currentQuicklists = this.props.quicklists.map((group)=>{
      if (group.id === this.props.currentGroup.id) {
        return self.props.currentGroup
      } else {
        return group
      }
    })
    //update the redux state
    this.props.updateQuicklists(currentQuicklists)
  }
  _addItem = (data)=>{
    const user = this.props.userInfo
    const itemData = database.dbObj.quickItem(data)
    //add the item to database
    database.quick.addItem(user, this.props.currentGroup, itemData ).then((item)=>{
      //add the item to currentGroup
      console.log("Item!", item)
      const updateGroup = this.props.currentGroup
      updateGroup.items.push(item)
      this.props.updateCurrentGroup(updateGroup)
    })
  }  
  _removeItem = (itemData)=>{
    database.quick.removeItem(this.props.userInfo, this.props.currentGroup ,itemData).then(()=>{
      const updatedItems = this.props.currentGroup.items.filter((item)=>{
        return item.id !== itemData.id
      })
      this.props.updateCurrentGroup({...this.props.currentGroup, items: updatedItems})
    })
  }
  _deleteList = ()=>{
    const message= `Are you sure you want to delete '${this.props.currentGroup.title}'? \nit will be gone forever!`
    const confirm = window.confirm(message)
    if(confirm === true){
      database.quick.deleteList(this.props.userInfo.email, this.props.currentGroup).then(()=>{
        this.props.updateQuicklists(this.props.quicklists.filter((group)=>{
          return this.props.currentGroup.id !== group.id
        }))
        this.props.history.push(PAGES.PATHS.QUICKLISTS)
      })
    }
  }
  _editSettings = ()=>{
    //toggles the 'edit list' settings
    if(this.state.settingsToggle  ){
      this._closeOverlay("settings")
      this.setState({
        settingsAction: {action:this._editSettings, svg:"edit", text:this.props.TEXT.EDIT}
      })
    } else {
      this.setState({
        settingsToggle:true,
        settingsAction: {action:this._editSettings, svg:"exit", text:this.props.TEXT.SAVE}
      })
    }
  }
  _addItemToGroup = ()=>{
    if(this.state.settingsToggle === true && this.state.selectGroupToggle === false){
      this.setState({
        settingsToggle:false,
        settingsAction: {action:this._editSettings, svg:"edit", text:this.props.TEXT.EDIT},
        headerAction: this.state.actionClose,
        selectGroupToggle: true,
      })
    } else {
        
      //close the edit list if open
      //toggles the select group menu
      this.setState({selectGroupToggle:true})
      this.setState({headerAction: this.state.actionClose})
    }
  }
  /* ---------------------------------------------------
    component mount conditions
  ---------------------------------------------------- */
  /* ---------------------------------------------------
    component render
  ---------------------------------------------------- */
  render(){
    return(
      <div className="wrapper single-quicklist">
        <DesktopNav />
        <section className="hide-mobile">
          <QuicklistsIndex />
        </section>
        <section className="quicklist-add-container">
          <Header 
            page="SINGLE_QUICKLIST" 
            settings={this.state.settingsAction}
            action={this.state.headerAction}
            />
          <this.QuicklistOverlays />
        </section>
        <Footer page="SINGLE_QUICKLIST" />
      </div>
    )
  }
}
/* ---------------------------------------------------
  Redux state and dispatch
---------------------------------------------------- */
const mapStateToProps = state => ({
  userGroups: state.userGroups,
  lang: state.language,
  currentGroup: state.currentGroup,
  userInfo: state.userInfo,
  quicklists: state.quicklists,
  TEXT: PAGES.TRANSLATE(state.language, 'SINGLE_QUICKLIST').SPECIFIC.SETTINGS
})
const mapDispatchToProps = (dispatch) => {
  return {
    //fetch the shopping lists and update the redux store 
    updateCurrentGroup: (data)=>{ dispatch( actions.currentGroup(data) ) },
    updateQuicklists: (data)=>{  dispatch(actions.updateQuicklists(data))},
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SingleQuicklist))
