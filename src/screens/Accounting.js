import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { actions } from '../reducer'
//Components
import Header from './components/Header'
import Footer from './components/Footer'
import ShowLists from './components/ShowLists'
import DesktopNav from './components/DesktopNav'

class Accounting extends React.Component {
  render(){
    //const PAGE = PAGES.TRANSLATE(this.props.lang, 'ACCOUNTING')
    return(
      <div className="wrapper">
        <DesktopNav />
        <section>
          <Header page="ACCOUNTING" />
          <ShowLists 
            page="accounting"
            arrayOfLists={this.props.userGroups} 
            linkTo='SINGLE_ACCOUNTING'
          /> 
          <Footer page="ACCOUNTING"/>
        </section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  reduxState: state,
  userGroups: state.userGroups,
  userInfo: state.userInfo,
  userStatus: state.isUserLoggedIn,
  lang: state.language,
  screenTransition: state.screenTransition,
})

const mapDispatchToProps = (dispatch) => {
  return {
    setScreenTransition: (state)=>{dispatch(actions.setScreenTransition(state))}
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Accounting))
