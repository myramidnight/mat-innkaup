import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {actions} from '../reducer'
//Constants
import PAGES from '../lib/constants/pages'
//Local Modules
import firebase from '../lib/modules/firebase'
import localStorage from '../lib/modules/localStorage'
//Components
import Header from './components/Header'
import Footer from './components/Footer'
import DesktopNav from './components/DesktopNav'
//Images
import SVG from './components/SVG'
import icons from '../lib/constants/icons'

class UserSettings extends React.Component {
  constructor(props){
    super(props)
    this.state = { 
    }
  }
  /* ---------------------------------------------------
    local components
    <div className="usersettings-container">
      <span className="subtitle">lykilord</span>
      <span>{this.props.userInfo.password}</span>
    </div>
  ---------------------------------------------------- */
  UserAccountSettings = ()=>{
    return(
      <div className="upper-container">
        <div className="usersettings-container">
          <span className="subtitle">{this.props.TEXT.USER.TITLE}</span>
          <span>{this.props.userInfo.displayName}</span>
        </div>
      </div>
    )
  }
  NoticesSettings = ()=>{
    return(
      <div className="usersettings-container-main">
        <span className="subtitle">{this.props.TEXT.NOTICE.TITLE}</span>
        <div>
          <span>{this.props.TEXT.NOTICE.SAVINGS}</span>
          <SVG data={icons.svg.toggle_orange}/>
        </div>
        <div>
          <span>{this.props.TEXT.NOTICE.DELIVERY}</span>
          <SVG data={icons.svg.toggle_orange}/>
        </div>
        <div>
          <span>{this.props.TEXT.NOTICE.GROUPS}</span>
          <SVG data={icons.svg.toggle_orange}/>
        </div>
      </div>
    )
  }
  LocationSettings = ()=>{
    return(
      <div className="usersettings-container">
        <span className="subtitle">{this.props.TEXT.LOCATION.TITLE}</span>
        <div>
          <span>{this.props.TEXT.LOCATION.TOGGLE_LOCATION}</span>
          <SVG data={icons.svg.toggle_orange}/>
        </div>
      </div>
    )
  }
  LanguageSettings = (props)=>{
    return(
      <div className="language-settings-container">
        <span className="subtitle">{this.props.TEXT.LANGUAGE['TITLE']}</span>
        <button onClick={()=>{this._changeLang()}}>{this.props.TEXT.LANGUAGE['LANG']}</button>
      </div>
    )
  }
  OtherSettings = (props)=>{
    return(
      <div className="usersettings-container-buttons">
        <button onClick={this._logout}>{this.props.TEXT.USER['LOGOUT']}</button>
        <button> Eyda adgang </button>
      </div>
    )
  }

  /* ---------------------------------------------------
    component functions
  ---------------------------------------------------- */
  _logout = ()=>{
    firebase.auth().signOut()
    console.log("User has been logged out")
    //force the user to login screen
    this.props.history.push(PAGES.PATHS.LOGIN)
  }
  _changeLang = ()=>{
    const currentLang = (lang)=>{ if( lang === 'is'){ return 'en'} else {return 'is'}  }
    //update current language in redux
    this.props.updateLang(currentLang(this.props.lang))
  }
  /* ---------------------------------------------------
    component mount conditions
  ---------------------------------------------------- */
  componentDidMount(){
  }
  componentWillUnmount(){
    localStorage.updateLocalState(this.props.reduxState)
  }
  /* ---------------------------------------------------
    component render
  ---------------------------------------------------- */
  render(){
    return(
      <div className="wrapper user-settings">
        <DesktopNav />
        <section>
          <Header page="USER_SETTINGS" />
          <this.UserAccountSettings />
          <this.NoticesSettings />
          <this.LocationSettings />
          <this.LanguageSettings />
          <this.OtherSettings />
          <Footer page="USER_SETTINGS" />
        </section>
      </div>
    )
  }
}
/* ---------------------------------------------------
  Redux state and dispatch
---------------------------------------------------- */
const mapStateToProps = state => ({
  reduxState: state,
  lang: state.language,
  userInfo: state.userInfo,
  TEXT: PAGES.TRANSLATE(state.language, 'USER_SETTINGS').SPECIFIC,
})
const mapDispatchToProps = (dispatch)=>{
  return {
    updateLang: (lang)=>{ dispatch(actions.updateLang(lang))}
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UserSettings))
