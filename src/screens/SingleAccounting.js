import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
//Components
import Header from './components/Header'
import Footer from './components/Footer'



class SingleAccounting extends React.Component {
  render(){
    return(
      <div className="wrapper">
        <Header title="Bókhald" desc="Listi yfir færslur"/>
        <p className="small-title-spacearound" >{this.props.currentGroup.title}</p>
        <ul>
          <li className="accounting-container">
            <span className="item1">Krónan</span>
            <span className="item2">Brynja Guðmundsdóttir</span>
            <span className="item3">5.760 kr</span>
            <span className="item4">24.04</span>
          </li>

          <li className="accounting-container">
            <span className="item1">Hagkaup</span>
            <span className="item2">Guðmundur Eyjolfsson</span>
            <span className="item3">4.200 kr</span>
            <span className="item4">20.04</span>
          </li>

          <li className="accounting-container">
            <span className="item1">Bónus</span>
            <span className="item2">Runar Johannsson</span>
            <span className="item3">11.500 kr</span>
            <span className="item4">19.04</span>
          </li>

          <li className="accounting-container">
            <span className="item1">10 - 11</span>
            <span className="item2">Diana Johanna Svavarsdóttir</span>
            <span className="item3">7.290 kr</span>
            <span className="item4">16.04</span>
          </li>

          <li className="accounting-container">
            <span className="item1">Iceland</span>
            <span className="item2">Guðmundur Eyjolfsson</span>
            <span className="item3">4.090 kr</span>
            <span className="item4">12.04</span>
          </li>
        </ul>
        <Footer page="SINGLE_ACCOUNTING"/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userInfo: state.userInfo,
  currentGroup: state.currentGroup,
  lang: state.language,
})

export default withRouter(connect(mapStateToProps)(SingleAccounting))
