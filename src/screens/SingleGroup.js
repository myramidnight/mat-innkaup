import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { actions } from '../reducer'
//Constants
import PAGES from '../lib/constants/pages'
import dbObj from '../lib/constants/dbObjects'
//Local modules
import database from '../lib/modules/database'
import localStorage from '../lib/modules/localStorage'
//Components
import Footer from './components/Footer'
import Header from './components/Header'
import ItemsList from './components/ItemsList'
import CreateNewItem from './components/CreateNewItem'
import GroupSettings from './subScreens/GroupSettings'
import GroupsIndex from './subScreens/GroupsIndex'
import DesktopNav from './components/DesktopNav'
//images
//import icons from '../lib/constants/icons'
//import SVG from './components/SVG'

class SingleGroup extends React.Component {
  //local state
  constructor(props){
    super(props)
    this.state = {
      settingsToggle: false,
    }
  }
  /* ---------------------------------------------------
    local components
  ---------------------------------------------------- */
  GroupMembers = (props)=>{
    const displayName = (member)=>{
      if(member.email === this.props.userInfo.email){
        return this.props.TEXT.YOU
      } else {
        return member.displayName.charAt(0).toUpperCase() ||member.email.slice(0,member.email.indexOf("@")).charAt(0).toUpperCase()
      }
    }
    return(
      <ul className="big-members-container">
        {props.members.map((member,i)=>{
          //if member doesn't have a displayName, use email instead
          return(
              <li className="membersContainer" key={i}>
                <span className="membersCircle" style={{backgroundColor:member.color}} >{displayName(member)}</span>
              </li>
            )
        })}
      </ul>)
  }
  /* ---------------------------------------------------
    component functions
  ---------------------------------------------------- */
  _confirmThis = (message, callback)=>{
    if( window.confirm(message) === true){ callback() }    
  }
  _addItem = (data)=>{
    database.items.addItem(this.props.currentGroup, dbObj.newItem(this.props.userInfo.email, data))
  }  
  _removeItem = (item)=>{
    database.items.removeItem(this.props.currentGroup,item)
  } 
  _settingsToggle = ()=>{
    if(this.state.settingsToggle){
      this.setState({settingsToggle:false})
    } else {
      this.setState({settingsToggle: true})
    }
  }
  _updateRedux = ()=>{
    const self = this
    //update the userGroups array with currentGroup changes
    const currentUserGroups = this.props.userGroups.map((group)=>{
      if (group.id === this.props.currentGroup.id) {
        return self.props.currentGroup
      } else {
        return group
      }
    })
    //update the redux state
    this.props.updateUserGroups(currentUserGroups)
    //update the localStorage with current 
  }
  _editSettings = ()=>{
    if(this.state.settingsToggle){
        this.setState({settingsToggle:false})
    } else {
      this.setState({settingsToggle:true})
    }
  }
  _copyListToQuicklist = ()=>{
      this._confirmThis(`Sure you want to copy ${this.props.currentGroup.title} to your quicklists?`, ()=>{
        return database.quick.createList(this.props.userInfo.email,this.props.currentGroup).then((newList)=>{
          const updateQuicklists = this.props.quicklists
          updateQuicklists.push(newList)
          this.props.updateQuicklists(updateQuicklists)
          const itemsList = this.props.currentGroup.items.map((item)=>{
            return database.quick.addItem(this.props.userInfo.email, newList, item)
          })
          return itemsList
        })
      })
  }
  /* ---------------------------------------------------
    component mount conditions
  ---------------------------------------------------- */
  componentDidMount(){
    //listen for live updates in group
    localStorage.updateLocalState(this.props.reduxState)
    //https://tylermcginnis.com/react-router-url-parameters/
  }
  componentWillUnmount(){
    //close modal if open
    this.setState({settingsToggle: false})
  }
  /* ---------------------------------------------------
    component render
  ---------------------------------------------------- */
  render(){
    const dbCollection = 'groups/' + this.props.currentGroup.id + '/items'
    return(
      <div className="wrapper">
        <DesktopNav/>
        <section className="hide-mobile">
          <GroupsIndex />
        </section>
        <section className="desktop-layout-container">
          <Header 
            titleAction={()=>{this._settingsToggle() }} 
            page="SINGLE_GROUP"
            title={this.props.currentGroup.title} 
            settings={{action:this._editSettings, svg:"gear", text:this.props.TEXT.SETTINGS.TITLE}}
            action={{action:this._copyListToQuicklist, svg:"star"}}
            />
          {/* <button><SVG data={icons.svg.add}/></button>*/}
          {this.state.settingsToggle ? (
            
            <GroupSettings toggleOff={()=>{this.setState({settingsToggle:false})}} />
          ):(
            <div>
              <this.GroupMembers members={this.props.currentGroup.access}/>
              <ItemsList 
                watchDb={dbCollection} 
                action={{
                  action:this._removeItem,
                  svg: "exit"}}
                updateRedux={this._updateRedux}
                checklist
              />
              <CreateNewItem action={this._addItem}/>
            </div>
          )}
        </section>
        <Footer page="SINGLE_GROUP" />
      </div>
    )
  }
}
/* ---------------------------------------------------
  Redux state and dispatch
---------------------------------------------------- */
const mapStateToProps = state => ({
  reduxState: state,
  userInfo: state.userInfo,
  userGroups: state.userGroups,
  currentGroup: state.currentGroup,
  quicklists: state.quicklists,
  lang: state.language,
  TEXT: PAGES.TRANSLATE(state.language, 'SINGLE_GROUP').SPECIFIC,
})
const mapDispatchToProps = (dispatch) => {
  return {
    //fetch the shopping lists and update the redux store 
    updateCurrentGroup: (data)=>{ dispatch( actions.currentGroup(data) ) },
    updateUserGroups: (data)=>{  dispatch(actions.userGroups(data))},
    updateQuicklists: (lists)=>{ dispatch(actions.updateQuicklists(lists))}
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SingleGroup))
