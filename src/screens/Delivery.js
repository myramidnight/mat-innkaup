import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
//Constants
//import PAGES from '../lib/constants/pages'
//Components
import Header from './components/Header'
import Footer from './components/Footer'
import DesktopNav from './components/DesktopNav'
//images
import icons from '../lib/constants/icons'
import SVG from './components/SVG'


class Delivery extends React.Component {
  constructor(props){
    super(props)
    this.state = { 
    }
  }

  StoreList = ()=>{
    return(
      <div>
        <div className="StoreList-container">
          <div className="title">
            <span className="title">Krónan</span>
            <span className="sidetitle">1 km</span>
          </div>
          <SVG data={icons.svg.forward}/> 
        </div>

        <div className="StoreList-container">
          <div className="title">
            <span className="title">Hagkaup</span>
            <span className="sidetitle">1,5 km</span>
          </div>
          <SVG data={icons.svg.forward}/> 
        </div>

        <div className="StoreList-container">
          <div className="title">
            <span className="title">Bónus</span>
            <span className="sidetitle">2,7 km</span>
          </div>
          <SVG data={icons.svg.forward}/> 
        </div>

        <div className="StoreList-container">
          <div className="title">
            <span className="title">10-11</span>
            <span className="sidetitle">5 km</span>
          </div>
          <SVG data={icons.svg.forward}/> 
        </div>

        <div className="StoreList-container">
          <div className="title">
            <span className="title">Bónus</span>
            <span className="sidetitle">10 km</span>
          </div>
          <SVG data={icons.svg.forward}/> 
        </div>
      </div>
      
    )
  }

  render(){
    //const PAGE = PAGES.TRANSLATE(this.props.lang, 'DELIVERY')
    return(
      <div className="wrapper">
        <DesktopNav />
        <section className="desktop-layout-stores">
          <Header page="DELIVERY" />
          <this.StoreList/>
          <Footer page="DELIVERY" />
        </section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  reduxState: state,
  userGroups: state.userGroups,
  userInfo: state.userInfo,
  userStatus: state.isUserLoggedIn,
  lang: state.language,
})

export default withRouter(connect(mapStateToProps)(Delivery))