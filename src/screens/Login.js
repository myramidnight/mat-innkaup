import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
//local modules
import firebase from '../lib/modules/firebase'
//Constants
import PAGES from '../lib/constants/pages'
//Components and Screens
import SignIn from './subScreens/SignIn'
import SignUp from './subScreens/SignUp'
// import { 
//   TimelineLite, 
// } from "gsap/all";//import { makeGenericClientConstructor } from 'grpc';
// // import { TransitionGroup, CSSTransition } from "react-transition-group";


class Login extends React.Component {
  _signInGoogle = ()=>{
    //signin with google
    firebase.loginGoogle("redirect").then(()=>{
      //redirect user afterwards
      this.props.history.push(PAGES.PATHS.HOME)
    }).catch((error)=>{
      console.log("ERROR while authenticating user: ", error)
    })
  }
  _signInEmail = (username, password)=>{
    //sign in with email + password
    firebase.signInWithEmail(username, password)
  }
  _signUpEmail = (email, password)=>{
    //create an account with email + password
    firebase.createUserWithEmail(email, password)
  }
  componentDidMount(){
    firebase.auth().onAuthStateChanged((user)=>{
      if(user){
        //if user is logged in, then redirect them
        this.props.history.push(PAGES.PATHS.HOME)
      }
    })
  }
   render(){
     return(
        <div className="wrapper">
          {this.props.toggle ? (
            <SignUp signUpEmail={this._signUpEmail}/>
          ):(
            <SignIn signInGoogle={this._signInGoogle} signInEmail={this._signInEmail}/>
          )}
        </div>
     )
   }

}
const mapStateToProps = state => ({
  toggle: state.toggle,
})
export default withRouter(connect(mapStateToProps)(Login))
