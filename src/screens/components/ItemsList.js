import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import icons from '../../lib/constants/icons'
import { actions } from '../../reducer'
//local modules
import database from '../../lib/modules/database'
//components
import SVG from './SVG'


class ItemsList extends React.Component {
  /* ---------------------------------------------------
    local components
  ---------------------------------------------------- */
  SingleListItem = (props)=>{
    const svgIcon = ()=>{
      if(this.props.action){
        return this.props.action.svg || "exit"
      } else {
        return "exit"
      }
    } 
    if(this.props.checklist ){
      return( 
        <li className="item-row">
          <input  type="checkbox" name={props.data.title} id={"item-"+ props.index}></input>
          <div className="check"></div>
          <label htmlFor={"item-"+ props.index}>{props.data.title}</label>
          <span className="item-quantity"> {props.data.quantity} {props.data.q_label}</span>
          <button className="remove-item" onClick={()=>{this.props.action.action(props.data)}}>
            <SVG data={icons.svg[svgIcon()]} className="remove-item"/>
          </button>
        </li>
      )
    } else if (this.props.quicklist ) {
      return( 
        <li className="item-row-quicklist">
          <span>{props.data.title}</span>
          <span className="item-quantity"> {props.data.quantity} {props.data.q_label}</span>
          <button className="remove-item" onClick={()=>{this.props.action.action(props.data)}}>
            <SVG data={icons.svg[svgIcon()]} className="remove-item"/>
          </button>
        </li>
      )
    } else {
      return(
        <li className="item-row">
          <div></div>
          <span>{props.data.title}</span>
          <span className="item-quantity"> {props.data.quantity} {props.data.q_label}</span>
          <button className="remove-item" onClick={()=>{this.props.action.action(props.data)}}>
            <SVG data={icons.svg[svgIcon()]} className="remove-item"/>
          </button>
        </li>
      )
    }
  }
  /* ---------------------------------------------------
    component functions
  ---------------------------------------------------- */
  _updateItemsList = (action, data)=>{
    const currentList = this.props.currentGroup.items
    if(action === "added"){
      //add the item to the array and update redux state
      currentList.push(data)
      this.props.updateCurrentGroup({...this.props.currentGroup , items: currentList})
    } else if (action === "modified"){
      //modify the item and update redux state

    } else if (action === "removed"){
      //remove the item from array and update redux state
      this.props.updateCurrentGroup({
        ...this.props.currentGroup, 
        items:currentList.filter((item)=>{
          return item.id !== data.id
        })
      })
    }
  }
  _callbackAction = (data)=>{
    this.props.action(data)
  }
  _watchForChanges = ()=>{
    console.log("Watching for changes in", this.props.currentGroup.title)
    //start the snapshot watcher for updates
    database.snapshots.itemsWatcher(this.props.watchDb, this.props.currentGroup.items, this._updateItemsList)
  }
  /* ---------------------------------------------------
    component mount conditions
  ---------------------------------------------------- */
  componentDidMount(){
    //listen for live updates in group
    if(this.props.watchDb){
      this._watchForChanges()
    }
  }
  componentWillUnmount(){
    //unsubscribe to the live listener
    database.collection('users').onSnapshot(()=>{})
    //updateReduxstate
    if(this.props.updateReux){
      this.props.updateRedux()
    }
  }
  /* ---------------------------------------------------
    component render
  ---------------------------------------------------- */
  render(){
    return(
      <ul className="items-list">
        {this.props.currentGroup.items ? (
          this.props.currentGroup.items.map((item, i)=>{
          return <this.SingleListItem key={i} index={i} data={item} action={this._callbackAction}/>
        })):(null)}
      </ul>
    )
  }
}
const mapStateToProps = state => ({
  userInfo: state.userInfo,
  currentGroup: state.currentGroup,
})
const mapDispatchToProps = (dispatch) => {
  return {
    //fetch the shopping lists and update the redux store 
    updateCurrentGroup: (data)=>{ dispatch( actions.currentGroup(data) ) },
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ItemsList))