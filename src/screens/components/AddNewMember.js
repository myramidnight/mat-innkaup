import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
//constants
import PAGES from '../../lib/constants/pages'
import icons from '../../lib/constants/icons'
//components
import SVG from './SVG'

class AddNewMember extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      formToggle : false,
    }
  }
  _submitForm = (event)=>{
    event.preventDefault() //prevents the page from refreshing
    const data = new FormData(event.target)
    const formData = {
      email: data.get('email'),
      status: data.get('status')
    }
    this.props.action(formData)
    document.getElementById('add-new-member').reset()
  }
  render(){
    return(
      <div>
        {this.state.formToggle ? (
          <form className="add-new-member" id="add-new-member" onSubmit={this._submitForm}>

          <div>
            <input name="email" type="text" placeholder="email" required/>
            <select name="status" defaultValue="normal">
              <option value="normal">{this.props.TEXT.MEMBERS.NORMAL}</option>
              {this.props.status === "admin" ? (<option value="admin">{this.props.TEXT.MEMBERS.ADMIN}</option>):(null)}
            </select>
          </div>

          <button><SVG data={icons.svg.add}/></button>
        </form>
        ):( 
          <button className="add-new-member" onClick={()=>{this.setState({formToggle:true})}}>
            <SVG data={icons.svg.add}/>
            <span>{this.props.TEXT.MEMBERS['ADD_MEMBERS']}</span>
          </button>
        )}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userInfo: state.userInfo,
  TEXT: PAGES.TRANSLATE(state.language, 'SINGLE_GROUP').SPECIFIC.SETTINGS,
})
export default withRouter(connect(mapStateToProps)(AddNewMember)) 