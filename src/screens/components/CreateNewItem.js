import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
//Constants
import PAGES from '../../lib/constants/pages'
//images
import icons from '../../lib/constants/icons'
import SVG from './SVG'

class CreateNewItem extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      formToggle : false,
    }
  }
  //Function that sends data in callback
  submitForm = (event)=>{
    //prevent the page from refreshing
    event.preventDefault() 
    const data = new FormData(event.target) 
    const formData = {
      title: data.get('title'),
      quantity: data.get('quantity'),
    }
    //send data with callback function
    this.props.action(formData)
    //reset the form
    document.getElementById('create-new-item').reset()
    //this.setState({formToggle: false})

  }
  render(){
    return(
      <div>
        {this.state.formToggle ? (
          <form className="add-items-form quicklist" id="create-new-item" onSubmit={this.submitForm}>
            <input type="text" name="title" placeholder={this.props.TEXT.PRODUCT.NAME} autoFocus required></input>
            <input type="text" name="quantity" placeholder={this.props.TEXT.PRODUCT.QUANTITY} required></input>
            <button><SVG data={icons.svg.add}/></button>
          </form>
        ):(
          <button className="add-items" onClick={()=>{this.setState({formToggle:true})}}>
            <SVG data={icons.svg.add}/>
            <span>{this.props.TEXT.PRODUCT.TITLE}</span>
          </button>
        )}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userInfo: state.userInfo,
  currentList : state.groups, // fetch the specified shopping list
  lang: state.language,
  TEXT: PAGES.TRANSLATE(state.language, 'INTERFACE'),
})
export default withRouter(connect(mapStateToProps)(CreateNewItem))