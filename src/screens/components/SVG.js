
import React from 'react'

class SVG extends React.Component{
  render(){
    const data = this.props.data || {paths:[]}
    const paths = data.paths || []
    //optional props
    const className = this.props.className || null
    const customFill = this.props.fill || null
    const customHeight = this.props.height || null
    const customWidth = this.props.width || null
    const customViewBox = this.props.viewBox || data.viewBox
    return(
      <svg className={className} height={customHeight} width={customWidth} viewBox={customViewBox} fill={customFill} xmlns="http://www.w3.org/2000/svg">
        {paths.map((path,i)=>{
          const fillPath = path.fill || null
          return(<path key={i} d={path} fill={fillPath}/>)
        })}
      </svg>
    )
  }
}

export default SVG