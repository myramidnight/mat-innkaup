import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import GridLoader from 'react-spinners/GridLoader';
import { css } from '@emotion/core';

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

class Loader extends React.Component {
  render(){
    //ID of authentication <div>
    if(this.props.loading) {
      return(
        <div className="loaderblock">
          <GridLoader
            css={override}
            sizeUnit={"px"}
            size={25}
            color={'#FF9E2D'}
            loading={this.props.loading}
          />
        </div>
      )
    } else {
      return null;
    }
  }


}
const mapStateToProps = (state )=>({
    userStatus : state.isUserLoggedIn,
    lang: state.language,
    loading: state.loading
})
export default withRouter(connect(mapStateToProps)(Loader))


