import React from 'react'
import { connect } from 'react-redux'
import { withRouter, Link } from 'react-router-dom'
import { actions } from '../../reducer'
import SVG from './SVG'
import icons from '../../lib/constants/icons'
//constants
import PAGES from '../../lib/constants/pages'

class ListOfGroups extends React.Component {
  /* ---------------------------------------------------
    local components
  ---------------------------------------------------- */
  ShowMembers = (props)=>{
    const TEXT = PAGES.TRANSLATE(this.props.lang,'INTERFACE').MEMBERS
    //shows the members SPAN element if available
    if(props.access && this.props.showMembers){
      return <span className="subtext">{props.access.length} {props.access.length > 1 ? (TEXT['PLURAL']):(TEXT['SINGLE'])}</span>
    } else {
      return null
    }    
  }
  ListItem = (props)=>{
    const showMembers = this.props.showMembers
    return(
      <li  className={showMembers ? ("show-members"):("no-members")}>
        <Link onClick={()=>{this._onClickAction(props.groupData)}} to={PAGES.PATHS[this.props.linkTo] + props.index}> 
          <div>
            <span className="title">{props.groupData.title}</span> 
            <this.ShowMembers access={props.groupData.access} />
          </div>
          <SVG data={icons.svg.forward}/> 
        </Link>
      </li>
    )
  }

  /* ---------------------------------------------------
    component functions
  ---------------------------------------------------- */
  _onClickAction = (group)=>{
    if(this.props.action){
      //return groupData with callback
      this.props.action(group)
    } else {
      //update currentGroup
      this.props.updateCurrentGroup(group)
    }
  }
  /* ---------------------------------------------------
    component render
  ---------------------------------------------------- */
  render(){
    //Render the component
    return(
      <ul className="groups-list">          
        {this.props.arrayOfLists ? ( //to prevent errors if arrayOfLists is not provided
          this.props.arrayOfLists.map((list,i)=>{
            return(<this.ListItem groupData={list} key={i} index={i}/>)
          })
        ):(
          null
        )}
      </ul>
    )
  }
}
const mapStateToProps = (state)=>({
  lang: state.language,
})
const mapDispatchToProps = (dispatch) => {
  return {
    updateCurrentGroup: (data)=>{ dispatch( actions.currentGroup(data) ) },
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListOfGroups))