import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { actions } from '../../reducer'
//Constants
import PAGES from '../../lib/constants/pages'
import icons from '../../lib/constants/icons'
//Components
import SVG from './SVG'

/*
This is the header component, 
it will render default texts and no toggles when used plain without properties.

To get the correct page titles, add the 'page' prop and define the page
based on their names in the `src/lib/constants/pageRoutes` 

To render the toggle buttons, then simply add the correct property,
there is no need to define value to the toggle props.  
<Header page="GROUPS" settings /> will render the settings toggle 
and default text for the 'groups' page.
*/

class Header extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      PAGE: PAGES.TRANSLATE(this.props.lang, this._defaultPage()) 
    }
  }
  /* ---------------------------------------------------
    local components
  ---------------------------------------------------- */
  HeaderTitle = ()=>{
    return(
      <h1 className="title" >{
        //If title is defined, render it, else use the default title 
        this.props.title ? ( this.props.title ) : (this.state.PAGE.TITLE)}
      </h1>
    )
  }
  HeaderSubtitle = ()=>{
    return(
      <h4 className="subtitle" >{
        //if the description is defined in props, use it, else use default
        this.props.desc ? ( this.props.desc ) : (this.state.PAGE.DESC)}
      </h4>
    )
  }
  ActionButton = ()=>{
    // action = {action: callbackFunction, svg: "name-of-svg"}
    const svgIcon = this.props.action.svg || "exit"
    return(
      <button className="header-action" onClick={()=>{this.props.action.action()}}><SVG className="close-nav" data={icons.svg[svgIcon]}/></button>
    )
  }
  SettingsButton = (props)=>{
    // settings = {action: callbackFunction, svg: "name-of-svg", text: "buttonText"}
    const svgIcon = this.props.settings.svg || "gear"
    const buttonText = this.props.settings.text || "stillingar"
    return(
      <button className="header-settings" onClick={()=>{this.props.settings.action()}}>{buttonText}<SVG data={icons.svg[svgIcon]}/></button>
    )
  }

  /* ---------------------------------------------------
    component functions
  ---------------------------------------------------- */
  _defaultPage = ()=>{
    if(this.props.page){
      //if the 'page' prop is defined
      return this.props.page.toUpperCase()
    } else {
      //if the 'page' prop is not defined, use this as default page
      return "HOME"
    } 
  }
  _settings = (toggle)=>{
    //if the 'settings' 
    if(toggle === false ){
      this.props.toggleModal(true)
    } else {
      this.props.toggleModal(false)
    }
  }

  /* ---------------------------------------------------
    component render
  ---------------------------------------------------- */
  render(){
    //The page text from 'pages' translation module. 
    //go to the 'src/lib/constants/translations' to change translated text
    
    const headerClasses = "header "
    return(
      <header className={headerClasses.concat(this.props.className || "")}>
        <div className="header-text">
          <this.HeaderTitle />
          {this.props.settings ? (
            //show edit-settings link button
            <this.SettingsButton/>):(
            //else show the subtitle
            <this.HeaderSubtitle />
          )}
        </div>
        {this.props.action ? (
          <this.ActionButton action={this.props.action.callback} svg={this.props.action.svg} />
        ):(null)}
      </header>
    )
  }
}
/* ---------------------------------------------------
  Redux state and dispatch
---------------------------------------------------- */
const mapStateToProps = state => ({
  toggle: state.toggle,
  lang: state.language,
})

const mapDispatchToProps = (dispatch)=>{
  return {
    updateLang: (lang)=>{ dispatch(actions.updateLang(lang))},
    toggleNav: (state)=>{dispatch(actions.toggleNav(state))},
    toggleModal: (state)=>{dispatch(actions.toggleModal(state))},
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header)) 