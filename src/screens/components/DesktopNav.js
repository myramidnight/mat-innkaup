import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
//components
import HomeNav from '../subScreens/HomeNav'

class DesktopNav extends React.Component {
  render(){
    //ID of authentication <div>
    //<span className="hide-mobile">{this.props.userInfo.displayName}</span>
    return(
      <div className="desktop-nav-container">
        <HomeNav className="desktop-nav hide-mobile" />
      </div>
    )
  }

}

const mapStateToProps = state => ({
  reduxState: state,
  lang: state.language,
  userInfo: state.userInfo,
})

export default withRouter(connect(mapStateToProps)(DesktopNav))


