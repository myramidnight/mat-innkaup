import React from 'react';
import { connect } from 'react-redux'
import { 
  withRouter, 
 // Link 
} from 'react-router-dom'
import { actions } from '../../reducer'
//Constants
//import PAGES from '../../lib/constants/pages'
import icons from '../../lib/constants/icons'
//Components
import SVG from './SVG'
import HomeNav from '../subScreens/HomeNav'
import Header from './Header'

import {
  CSSTransition,
} from 'react-transition-group';


/*
This is the footer component, it will render the navigation.
Please include it on all pages, else the user might be unable to navigate
between pages

If a specifc page needs to have a 'forward' button, 
then simply add the forward property, and you give it the function
it shall run as a value. <Footer forward={myFunction} /> 
*/

// create buttom tween

class Footer extends React.Component {    
  /* ---------------------------------------------------
    component functions
  ---------------------------------------------------- */
  _backButton = ()=>{
    this.props.setScreenTransition("slideBack")
    this.props.history.goBack()
    this.props.updateToggleNav(false)
  }
  _navButton = ()=>{
    this.props.setScreenTransition("fade")
    this.props.updateCurrentScreen(this.props.page)
    this.props.updateToggleNav(true)
  }
  _closeNav = ()=>{
    this.props.updateCurrentScreen(false)
    this.props.updateToggleNav(false)
  }
  /* ---------------------------------------------------
    component render
  ---------------------------------------------------- */
  render(){
    return(
      <footer className="footer hide-desktop">
        <nav>
          <CSSTransition appear={true} in={this.props.currentScreen !== this.props.page} timeout={400} classNames="show-nav">
            <div className="nav-toggle">
              <Header 
                page="HOME" title={this.props.userInfo.displayName} 
                action={{action:()=>{this._closeNav()}, svg: "exit"}}
                />
              <HomeNav className="home-nav" />
             {/*<Link className="showBtn" to={PAGES.PATHS.GUIDE} > Sýnikennsla</Link>  */} 
            </div>
          </CSSTransition>
            <ul className={this.props.toggleNav ? ("footer-nav hide-nav"):("footer-nav")}>
              <li><button onClick={()=>{this._backButton()}}> <SVG className="icon" data={icons.svg.backward}/> </button></li>
              <li><button onClick={()=>{this._navButton()}} ><SVG className="icon" data={icons.svg.menu}/></button></li>
              <li>{this.props.forward ? (
                //render the 'forward' button if the property is defined.
                <button 
                onClick={()=>{this.props.setScreenTransition("slideForward"); this.props.forward()}} ><SVG className="icon" data={icons.svg.forward}/></button>
              ):(
                //render nothing
                null
              )}</li>
            </ul>
        </nav>
      </footer>
    )
  }
}
/* ---------------------------------------------------
  Redux state and dispatch
---------------------------------------------------- */
const mapStateToProps = state => ({
  lang: state.language,
  toggleNav: state.toggleNav,
  userInfo: state.userInfo,
  currentScreen: state.currentScreen
})
const mapDispatchToProps = (dispatch)=>{
  return {
    updateToggleNav :(state)=>{ dispatch(actions.toggleNav(state))},
    setScreenTransition: (state)=>{dispatch(actions.setScreenTransition(state))},
    updateCurrentScreen: (screen) => {dispatch(actions.updateCurrentScreen(screen))}
  }
}
export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Footer)) 
