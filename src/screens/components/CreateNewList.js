import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
//Constants
import icons from '../../lib/constants/icons'
//components
import SVG from './SVG'

class CreateNewList extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      formToggle : false,
    }
  }
  _submitForm = (event)=>{
    event.preventDefault() //prevents the page from refreshing
    const data = new FormData(event.target) //target the form data
    //the information that will be saved to database
    const formData = {
      title: data.get('title')
    }
    //return the data with callback
    this.props.action(formData)
    //reset the form 
    //this.state.theForm.reset()
    this.setState({formToggle: false})
  }
  render(){
    const placeholderText = this.props.placeholderText || "List name"
    const toggleText = this.props.toggleText || "Add new list"
    return(
      <div>
        {this.state.formToggle ? (
            <form className="add-group-form" id="create-new-list" onSubmit={this._submitForm}>
              <label><input type="text" name="title" placeholder={placeholderText} autoFocus required></input></label>
              <button><SVG data={icons.svg.add}/></button>
            </form>
        ):(
          <button className="add-group" onClick={()=>{
            this.setState({formToggle: true})
          }}>
            <SVG data={icons.svg.add}/>
            <span>{toggleText}</span>
          </button> 
        )}
      </div>
    )
  }
}

//<document onClick={()=>{this.setState({formToggle: true})}}></document>

export default withRouter(connect(null)(CreateNewList))