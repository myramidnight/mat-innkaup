import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import {
  CSSTransition,
  TransitionGroup,
} from 'react-transition-group';

class AnimSladeLeft extends React.Component {
  /* ---------------------------------------------------
    component render
  ---------------------------------------------------- */
  render(){
    //Render the component
    return(
      <TransitionGroup>
        <CSSTransition
          key={this.props.location.key}
          timeout={1450}
          classNames="fade"
        >
        {this.props.children}
        </CSSTransition>
      </TransitionGroup>
    )
  }
}


export default withRouter(connect(null)(AnimSladeLeft))