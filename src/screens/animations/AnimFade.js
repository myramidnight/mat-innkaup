import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import {
  CSSTransition,
  TransitionGroup,
} from 'react-transition-group';

class AnimFade extends React.Component {
  /* ---------------------------------------------------
    component render
  ---------------------------------------------------- */
  render(){
    //Render the component
    return(
      <TransitionGroup>
        <CSSTransition
          key={this.props.location.pathname}
          timeout={290}
          classNames={this.props.screenTransition}
        >
        {this.props.children}
        </CSSTransition>
      </TransitionGroup>
    )
  }
}

const mapStateToProps = state => ({
  screenTransition: state.screenTransition
})

export default withRouter(connect(mapStateToProps)(AnimFade))