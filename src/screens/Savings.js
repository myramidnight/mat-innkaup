import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { actions } from '../reducer'
//Constants
//import PAGES from '../lib/constants/pages'
//Components
import Header from './components/Header'
import Footer from './components/Footer'
import ShowLists from './components/ShowLists'
import DesktopNav from './components/DesktopNav'


class Savings extends React.Component {
  componentDidMount() {
    console.log("savings componentDidMount()")
    setTimeout(() => {
      if(this.props.screenTransition !== 'slideForward') {
        this.props.setScreenTransition("slideForward")    
      }
    }, 350);
  }

  render(){
    //const PAGE = PAGES.TRANSLATE(this.props.lang, 'SAVINGS')
    return(
      <div className="wrapper">
        <DesktopNav />
        <section>
          <Header page="SAVINGS" />
          <ShowLists 
            page="savings"
            arrayOfLists={this.props.userGroups} 
            linkTo='SINGLE_ACCOUNTING'
          />
          <Footer page="SAVINGS" />
        </section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userGroups: state.userGroups,
  lang: state.language,
  screenTransition: state.screenTransition,
})

const mapDispatchToProps = (dispatch) => {
  return {
    setScreenTransition: (state)=>{dispatch(actions.setScreenTransition(state))}
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Savings))
