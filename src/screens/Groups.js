import React from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { actions } from '../reducer'
//local modules
import database from '../lib/modules/database'
//components
import Footer from './components/Footer'
import GroupsIndex from './subScreens/GroupsIndex'
import DesktopNav from './components/DesktopNav'


class Groups extends React.Component {
  _updateGroupsList = (action, data)=>{

    console.log("GroupWatcher action " + action, data)
  }
  _watchForChanges = ()=>{
    database.snapshots.groupsWatcher(this.props.userInfo,this.props.userGroups, this._updateGroupsList)
  }
  /* ---------------------------------------------------
    component mount conditions
  ---------------------------------------------------- */
  componentDidMount() {
    this._watchForChanges()
    setTimeout(() => {
      if(this.props.screenTransition !== 'slideForward') {
        this.props.setScreenTransition("slideForward")    
      }
    }, 350);
  }

  /* ---------------------------------------------------
    component render
  ---------------------------------------------------- */
  render(){
    return(
      <div className="wrapper groups">
        <DesktopNav />
        <GroupsIndex />
        <Footer page="GROUPS"/>
      </div>
    )
  }
}
/* ---------------------------------------------------
  Redux state and dispatch
---------------------------------------------------- */
const mapStateToProps = state => ({
  screenTransition: state.screenTransition,
  userInfo: state.userInfo,
  userGroups: state.userGroups,
})

const mapDispatchToProps = (dispatch) => {
  return {
    setScreenTransition: (state)=>{dispatch(actions.setScreenTransition(state))}
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Groups))
