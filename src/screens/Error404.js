import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
//Constants
//import PAGES from '../lib/constants/pages'
//Components
import Header from './components/Header'
import Footer from './components/Footer'

class Error404 extends React.Component {
  render(){
    //const PAGE = PAGES.TRANSLATE( 'Error404',this.props.lang)
    return(
      <div className="wrapper">
        <Header page="ERROR_404" />
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  lang: state.language,
})
export default withRouter(connect(mapStateToProps)(Error404))