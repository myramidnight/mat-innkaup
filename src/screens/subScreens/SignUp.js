import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { actions }  from '../../reducer'
//constants
import PAGES from '../../lib/constants/pages'
import icons from '../../lib/constants/icons'
//components
import SVG from '../components/SVG'
class SignUp extends React.Component {
  _submitForm = (event)=>{
    event.preventDefault() //prevents the page from refreshing
    const data = new FormData(event.target)
    const email = data.get('username')
    const pass = data.get('password')
    const confirm = data.get('confirm')
    //if the passwords matched
    if ( pass === confirm ){
      this.props.signUpEmail(email, pass)
    } else {
      alert("Passwords didn't match")
    }
  }

  render(){
    const TEXT = PAGES.TRANSLATE(this.props.lang, 'LOGIN').SPECIFIC
    return(
      <div className="sign-up">

        <section className="call-to-action-signup">
        <SVG className="logo" data={icons.svg.logo}/>
          <div className="flavorText-desktop">
            <div className="flavorText">
              <h3>{TEXT['WELCOME']}</h3>
              <p> ef þú ert með aðgang, skráðu þig inn og njóttu </p>
            </div>

            <button  ref={ x => this.CalltoActionContainer2 = x } 
              onClick={()=>{this.props.toggleModal(false)}}>{TEXT['SIGNIN']} 
            </button>
          </div>

          <div className="flavorText-mobile">
            <div className="flavorText">
              <h3> Velkomin aftur</h3>
              <p> Skráðu þig inn og  njóttu</p>
            </div>

            <button
              onClick={()=>{this.props.toggleModal(false)}}> {TEXT['SIGNIN']} 
            </button>
          </div>

        </section>

        <section className="white-box">
          <SVG data={icons.svg.logo} className="logo"/>
          <div className="login-form-container">
            <form className="login" onSubmit={this._submitForm}>
              <input type="email" name="username" placeholder={TEXT['USERNAME']} required/> 
              <input type="password" name="password" placeholder={TEXT['PASS']} required/>
              <input type="password" name="confirm" placeholder={TEXT['PASS_CONFIRM']} required/>
              <button>{TEXT['SUBMIT_SIGNUP']}</button>
            </form>
          </div>
        </section>
      </div>
    )
  }
}

const mapStateToProps = (state )=>({
  userStatus : state.isUserLoggedIn,
  lang: state.language
})
const mapDispatchToProps = (dispatch) => {
  return {
    toggleModal: (status) => { dispatch(actions.toggleModal(status))}
  }
}
export default withRouter(connect(mapStateToProps,mapDispatchToProps)(SignUp))