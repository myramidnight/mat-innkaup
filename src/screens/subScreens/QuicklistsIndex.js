import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { actions } from '../../reducer'
//Local Modules
import db from '../../lib/modules/database'
//Constants
import PAGES from '../../lib/constants/pages'
//Components
import ShowLists from '../components/ShowLists'
import CreateNewList from '../components/CreateNewList'
import Header from '../components/Header'

class QuicklistsIndex extends React.Component {
  _addNewList = (listData)=>{
    db.quick.createList(this.props.userInfo.email, listData ).then((list)=>{
      const newList = this.props.quicklists
      newList.push(list)
      this.props.updateQuicklists(newList)
    })
  }
  render(){
    return(
      <div className="desktop-layout-container">
        <div className={this.props.className||null}>
          <Header page="QUICKLISTS"/>
          <ShowLists
            arrayOfLists={this.props.quicklists} 
            linkTo="SINGLE_QUICKLIST" 
          />
          <CreateNewList 
            toggleText={this.props.TEXT['QUICKLIST']}
            placeholderText={this.props.TEXT['NAME']}
            action={this._addNewList}
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userInfo: state.userInfo,
  lang: state.language,
  quicklists: state.quicklists,
  TEXT: PAGES.TRANSLATE(state.language, 'INTERFACE'),
})
const mapDispatchToProps = (dispatch) => {
  return {
    updateQuicklists: (data)=>{ dispatch( actions.updateQuicklists(data) ) },
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(QuicklistsIndex))