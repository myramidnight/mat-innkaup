import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { actions }  from '../../reducer'
//Constants
import PAGES from '../../lib/constants/pages'
import icons from '../../lib/constants/icons'
//components
import SVG from '../components/SVG'
import Loader from '../components/Loader'

class SignIn extends React.Component {
  PAGE = (page)=>(PAGES.TRANSLATE(this.props.lang, page))
  /* update the database with user info */
  _submitForm = (event)=>{
    event.preventDefault() //prevents the page from refreshing
    const data = new FormData(event.target)
    this.props.signInEmail(data.get('username'), data.get('password'))
  }
  render(){
    //ID of authentication <div>
    const TEXT = PAGES.TRANSLATE(this.props.lang, 'LOGIN').SPECIFIC
    return(
      <div className="sign-in">
      <Loader />
        <section className="white-box">
          <SVG data={icons.svg.logo} className="logo"/>
          <div className="login-form-container">
            <form className="login" onSubmit={this._submitForm}>
              <label className="inp">
                <input type="email" name="username" required/> 
                <span className="label">{TEXT['USERNAME']}</span>
                <span className="border"></span>
              </label>
              <label className="inp">
               <input type="password" name="password" required/>
               <span className="label">{TEXT['PASS']}</span>
                <span className="border"></span>
              </label>
              <button>{TEXT['SUBMIT_SIGNIN']}</button>
            </form>

            <span className="eda">{TEXT['OR']}</span>
            <button 
              onClick={()=>{this.props.setLoading(true);this.props.signInGoogle()}}>{TEXT['GOOGLE']}
            </button>
          </div>
        </section>

        <section className="call-to-action-signin">
          <SVG className="logo" data={icons.svg.logo}/>
          
          <div className="flavorText-desktop" >
            <div className="flavorText">
              <h3> Taktu skrefið</h3>
              <p> búðu til aðgang og upplifðu einfaldari matrinnkaup</p>
            </div>

            <button ref={ x => this.CalltoActionContainer = x }
              onClick={()=>{this.props.toggleModal(true)}}>{TEXT['REGISTER']}
            </button>
          </div>

          <div className="flavorText-mobile" >
            <div className="flavorText">
              <h3> Taktu skrefið</h3>
              <p> Upplifðu einföld matarinnkaup</p>
            </div>

            <button 
              onClick={()=>{this.props.toggleModal(true)}}>{TEXT['REGISTER']}
            </button>
          </div>

        </section>
      </div>
    )
  }


}
const mapStateToProps = (state )=>({
    userStatus : state.isUserLoggedIn,
    lang: state.language,
})
const mapDispatchToProps = (dispatch) => {
  return {
    //fetch the shopping lists and update the redux store
    updateUserInfo: ( data )=>{ dispatch( actions.updateUserInfo( data ) )},
    toggleModal: (status) => { dispatch(actions.toggleModal(status))},
    setLoading: (isloading)=>{dispatch(actions.setLoading(isloading))},
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SignIn))


