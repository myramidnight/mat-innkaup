import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { actions } from '../../reducer'
import database from '../../lib/modules/database'
import SVG from '../components/SVG'
import icons from '../../lib/constants/icons'

class AddQuicklist extends React.Component {
  //local state
  constructor(props){
    super(props)
    this.state = {
      currentScreen: this.ShowUserGroups, //the initial component
      selectedGroup: {},
      currentList: this.props.currentGroup,
      step: 1, //updates what step of the action we're at
    }
  }
  /* ---------------------------------------------------
    local components
  ---------------------------------------------------- */
  CurrentItems = ()=>{
    return(
    <div>
      <ul className="quick-list-add">
        { this.props.currentGroup.items.map((item, i)=>{
          return(<li key={i}>
            <span>{item.title}</span> 
            <span>{item.quantity}</span></li>)
        })}
      </ul>
    </div>
    )
  }
  Step1 = ()=>{
    return(
      <ul className="quickList-add-group">
        {this.props.userGroups.map((list,i)=>{
          return(<li key={i} onClick={()=>{this._selectGroup(list)}}>{list.title} <SVG data={icons.svg.forward}/> </li>)
        })}
      </ul>
    )
  }
  Step2 = (group)=>{
    console.log("Group!",group)
    return(
      <div>
        <h3 className="title">{group.data.title} </h3>
          <span className="subtitle">sem innheldur eftirfarandi:</span>
            {group.data.items.length > 0 ? (
              <ul className="quick-list-add">
                {group.data.items.map((item,i)=>{
                  return(<li key={i}>
                    <span>{item.title}</span> 
                    <span>{item.quantity}</span></li>)
                })}
              </ul>
            ):(
              <li>Engar vörur</li>
            )}
        <button className="delete-list" onClick={()=>{this._confirmItems()}}>Bæta vörum</button>
      </div>
    )
  }
  SwitchSteps = ()=>{
    //this switches components to display
    switch(this.state.step){
      case 1:
      default:
        return <this.Step1 />
      case 2: 
        return <this.Step2 data={this.state.selectedGroup}/>
      case 3: 
        return <this.Step3 />
    }
  }
  /* ---------------------------------------------------
    component functions
  ---------------------------------------------------- */
  _confirmThis = (message, callback)=>{
    //asks user to confirm their action
    if( window.confirm(message) === true){ callback() }    
  }
  _selectGroup = (listData)=>{
    //trigger the next step
    this.setState({
      step:2,
      selectedGroup: listData,
    })
  }
  _confirmItems = ()=>{
    //confirm if you want to add the items to specified group
    this._confirmThis(`Ertu viss að þú viljir bæta þessum vörum við '${this.state.selectedGroup.title}'?`, ()=>{
      database.quick.addToGroup(this.state.selectedGroup, this.props.currentGroup.items)
    })
  }
  
  /* ---------------------------------------------------
    component mount conditions
  ---------------------------------------------------- */
  componentWillUnmount(){
    const self = this
    //update the userGroups array with currentGroup changes
    const currentQuicklists = this.props.quicklists.map((group)=>{
      if (group.id === this.props.currentGroup.id) {
        return self.props.currentGroup
      } else {
        return group
      }
    })
    //update the redux state
    this.props.updateQuicklists(currentQuicklists)
  }
  /* ---------------------------------------------------
    component render
  ---------------------------------------------------- */
  render(){
    return(
      <div  className="quickList-add-container">
        <h4 className="subtitle">bæta eftirfarndi vörum</h4>
        <this.CurrentItems />
        <h4 className="subtitle">við innkaupalistann</h4>
        <div id="add-to-group">
          <this.SwitchSteps />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userGroups: state.userGroups,
  lang: state.language,
  currentGroup: state.currentGroup,
  userInfo: state.userInfo,
  quicklists: state.quicklists
})
const mapDispatchToProps = (dispatch) => {
  return {
    //fetch the shopping lists and update the redux store 
    updateCurrentGroup: (data)=>{ dispatch( actions.currentGroup(data) ) },
    updateQuicklists: (data)=>{  dispatch(actions.updateQuicklists(data))},
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddQuicklist))