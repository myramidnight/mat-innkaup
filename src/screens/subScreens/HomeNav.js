import React from 'react'
import { connect } from 'react-redux'
import { withRouter, Link } from 'react-router-dom'
import { actions } from '../../reducer'
//Constants
import PAGES from '../../lib/constants/pages'
import icons from '../../lib/constants/icons'
//Components
import SVG from '../components/SVG'

class HomeNav extends React.Component {
  PAGE = (page)=>(PAGES.TRANSLATE(this.props.lang, page))
  NavLink = ({item})=>{
    return(
      <li>
        <Link to={item.path} className="main-nav-link">
          <SVG className="menu-icon" data={item.svg}/>
          <span>{item.title}</span>
        </Link>
      </li>
    ) 
  }
  
  /* ---------------------------------------------------
    component functions
  ---------------------------------------------------- */
  _listOfLinks = ()=>{
    //list of navigation items

    //{ path: PAGES.PATHS['SAVINGS'],
    //title: this.PAGE('SAVINGS').SHORT_TITLE,
    //svg: icons.svg.piggy },
    //{ path: PAGES.PATHS['ACCOUNTING'],
    //title: this.PAGE('ACCOUNTING').SHORT_TITLE,
    //svg: icons.svg.coin },

    return [
      { path: PAGES.PATHS['GROUPS'],
        title: this.PAGE('GROUPS').SHORT_TITLE,
        svg: icons.svg.list},
      { path: PAGES.PATHS['QUICKLISTS'],
        title: this.PAGE('QUICKLISTS').SHORT_TITLE,
        svg: icons.svg.starFill},
        { path: PAGES.PATHS['DELIVERY'],
        title: this.PAGE('DELIVERY').SHORT_TITLE,
        svg: icons.svg.shop },
      { path: PAGES.PATHS['USER_SETTINGS'],
        title: this.PAGE('USER_SETTINGS').SHORT_TITLE,
        svg: icons.svg.userFill },
    ]
  }
  /* ---------------------------------------------------
    component mount conditions
  ---------------------------------------------------- */
  componentWillUnmount() {
    this.props.toggleNav(false);
  }
  /* ---------------------------------------------------
    component render
  ---------------------------------------------------- */
  render(){
    return(
      <ul className={this.props.className}>
        {this._listOfLinks().map((navItem,i)=>{
          return <this.NavLink key={i} item={navItem}/>
        })}
      </ul>
    )
  }
}
/* ---------------------------------------------------
  Redux state and dispatch
---------------------------------------------------- */
const mapStateToProps = state => ({
  userInfo : state.userInfo, // fetch the specified shopping list
  lang: state.language,
})
const mapDispatchToProps = (dispatch) => {
  return {
    toggleNav: (state)=>{dispatch(actions.toggleNav(state))},
    setScreenTransition: (state)=>{dispatch(actions.setScreenTransition(state))}
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HomeNav))
