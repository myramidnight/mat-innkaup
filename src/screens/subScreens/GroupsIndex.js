import React from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { actions } from '../../reducer'
//Local Modules
import db from '../../lib/modules/database'
import { updateLocalState } from '../../lib/modules/localStorage'
//Constants
import PAGES from  '../../lib/constants/pages'
//components
import ShowLists from '../components/ShowLists'
import CreateNewList from '../components/CreateNewList'
import Header from '../components/Header'


class GroupsIndex extends React.Component {
  /* ---------------------------------------------------
    component functions
  ---------------------------------------------------- */
  _addNewGroup = (groupData)=>{
    const user = this.props.userInfo
    const groupInfo = db.dbObj.newGroup(user, groupData)
    //add the new group to database
    db.groups.addGroup(user,groupInfo).then((newGroup)=>{
      console.log("New Group: ", newGroup)
      const currentGroups = [...this.props.userGroups, newGroup]
      //add the new group to the userGroups list in redux state
      this.props.updateUserGroups(currentGroups)
    }).then(()=>{
      //update the localStorage with the latest userGroups from redux state
      updateLocalState(this.props.reduxState)
    })
  }

  /* ---------------------------------------------------
    component render
  ---------------------------------------------------- */
  render(){
    const TEXT = PAGES.TRANSLATE(this.props.lang, 'INTERFACE').LISTS
    return(
      <div className="desktop-layout-container">
        <div className={this.props.className||null}>
          <Header 
            page="GROUPS"
          />
          <ShowLists 
            arrayOfLists={this.props.userGroups} 
            linkTo="SINGLE_GROUP" 
            showMembers
          />
          <CreateNewList 
            placeholderText={TEXT['GROUP']}
            toggleText={TEXT['TITLE']}
            action={this._addNewGroup}
          />
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  reduxState: state,
  userInfo: state.userInfo,
  userGroups: state.userGroups,
  lang: state.language,
})
const mapDispatchToProps = (dispatch) => {
  return {
    updateUserGroups: (data)=>{ dispatch( actions.userGroups(data) ) },
  }
}
export default withRouter(connect(mapStateToProps,mapDispatchToProps)(GroupsIndex))