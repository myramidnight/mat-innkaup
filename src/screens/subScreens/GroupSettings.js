import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { actions } from '../../reducer'
//Local Modules
import db from '../../lib/modules/database'
//Constants
import PAGES from '../../lib/constants/pages'
import icons from '../../lib/constants/icons'
//components
import AddNewMember from '../components/AddNewMember'
//import Header from '../components/Header'
import SVG from '../components/SVG'

class GroupSettings extends React.Component {
  /* ---------------------------------------------------
    local components
  ---------------------------------------------------- */
  /*  DateCreated = ()=>{
    //returns the date string of when the group was created
    const months = ["Jan", "Feb", "Mar", "Apr", "Maí", "Jun", "Júl", "Águ", "Sept", "Nov", "Des" ]
    const date = new Date(this.props.currentGroup.created)
    return( <span className="group-created">{date.getDate() + ". " + months[date.getUTCMonth()] + " " + date.getFullYear()}</span>)
  }*/
  ListMembers = ()=>{
    //returns a list of members
    return(
      <ul className="members-list">
        {this.props.currentGroup.access.map((member, i)=>{
          //render each member
          return(
            <li className="group-settings-members-list" key={i}>
              <div>
                <span className="member-name">{member.displayName}</span> 
                <p className="small-title">{member.status}</p>
              </div>
              <button onClick={()=>{this._removeMember(member)}}><SVG data={icons.svg.trash} /></button>
            </li>
          )
        })}
      </ul>) 
  }
  /* ---------------------------------------------------
    component functions
  ---------------------------------------------------- */
  _confirmThis = (message, callback)=>{
    if( window.confirm(message) === true){ callback() }    
  }
  _updateUserGroups = (member, action)=>{
    const self = this
    const promise = new Promise((resolve)=>{
      const updateRedux = (updatedGroup)=>{
        console.log("Update redux, "+ action, updatedGroup)
        
        const updatedUserGroups = self.props.userGroups.map((group)=>{
          if(group.id === self.props.currentGroup.id){
            //replace group matching updatedGroup id with new object
            console.log("userGroups array updated")
            return updatedGroup
          } else {
            return group
          }
        })
        //update the userGroups in redux state
        this.props.updateCurrentGroup(updatedGroup)
        this.props.updateUserGroups(updatedUserGroups)
      }

      const updatedGroup = this.props.currentGroup
      if(action==='add'){
        //add the member to the group
        console.log("Adding..", member.displayName)
        updatedGroup.access.push(member)
        updatedGroup.members.push(member.email)
        updateRedux(updatedGroup)
      } 
      if (action === 'remove'){
        //filter the member out of group
        console.log("Removing..", member.displayName)
        updatedGroup['access'] = updatedGroup.access.filter((user)=>{ return user.email !== member.email })
        updatedGroup['members'] = updatedGroup.members.filter((user)=>{ return user !== member.email })
        updateRedux(updatedGroup) 
      }
    }).then((updatedGroup)=>{
      console.log("Updated group, ", updatedGroup)
    }).catch((error)=>{
      console.log("ERROR in _updateUserGroups:", error)
    })
    return promise
  }
  _removeMember = (member)=>{
    const self = this
    if (member.status === 'normal'){
      this._confirmThis(`Do you really wish to remove this member? \nTheir status is ${member.status}.`, ()=>{
        db.members.removeMembers(self.props.userInfo,  [member], self.props.currentGroup).then((results)=>{
          console.log("User has been removed", results)
        })
        self._updateUserGroups(member, 'remove')
      })
    }
    if (member.status === 'admin'){
      alert("This user is an admin, they cannot be removed")
    }
  }
  _addNewMember = (newUser)=>{
    const self = this
    //As long as the email doesn't match the current user, then try to add the new member
    if (newUser.email === this.props.userInfo.email ){
      //if the provided email is the current user
      return console.log("You can't add yourself")
    } else if (this.props.currentGroup.members.includes(newUser.email)){
      //if user is already in group
      return console.log("User already in group")
    } else {
      //tell the database to add the new member to the group
      return db.members.addMember(self.props.userInfo.email, newUser, self.props.currentGroup).then((newMember)=>{
        if(newMember === false){
          console.log('Email does not match any existing user')
        } else {
          this._updateUserGroups(newMember, 'add')
        }
      })
    }
  }
  _deleteGroup = ()=>{
    const numItems = this.props.currentGroup.items || []
    this._confirmThis(`Do you really wish to delete this group? \nIt currently has ${numItems.length} items.`, ()=>{
      //delete the group from database
      db.groups.deleteGroup(this.props.currentGroup).then(()=>{
        //update the userGroups in redux
        this.props.updateUserGroups(this.props.userGroups.filter((group)=>{
          return group.id !== this.props.currentGroup.id
        }))
        //push the user back to groups page
        this.props.history.push(PAGES.PATHS.GROUPS)
      })
    })
  }
  _deleteAllItems = ()=>{
    const listOfItems = this.props.currentGroup.items
    const currentGroup = this.props.currentGroup
    this._confirmThis(`Do you really wish to delete all items from this group's list? \nIt currently has ${this.props.currentGroup.items.length} items.`, ()=>{
      return listOfItems.map((item)=>{
        //delete all items from the group
        return db.items.removeItem(currentGroup, item).then((results)=>{
          //close the settings overlay
          this.props.toggleOff()
        })
      })
    })
  }
  /* ---------------------------------------------------
    component render
  ---------------------------------------------------- */
  render(){ 

    //gets the translated strings specific to the settings panel
    return(
      <div className="group-settings">
        <p className="small-title-top" >{this.props.TEXT.MEMBERS['MEMBERS_LIST']}</p>
        <section className="group-info">
          <this.ListMembers />
        </section>
        { //as long as the user is a moderator/admin (not 'normal'), then render this
          this.props.currentGroup.yourAccess === 'admin' ? (<AddNewMember status={this.props.currentGroup.yourAccess} action={this._addNewMember}/>):(null)}
        { //if the user is 'admin', then render this section
          this.props.currentGroup.yourAccess === 'admin' ? (
          <section className="eyda">
            <button onClick={()=>{ this._deleteAllItems()}}>{this.props.TEXT['DELETE_ITEMS']}</button>
            <button onClick={()=>{ this._deleteGroup()}}>{this.props.TEXT['DELETE_LIST']}</button>
          </section>
        ):(null)}
      </div>
    )
  }
}
const mapStateToProps = state => ({
  userInfo: state.userInfo,
  lang: state.language,
  currentGroup: state.currentGroup,
  userGroups: state.userGroups,
  TEXT: PAGES.TRANSLATE(state.language, 'SINGLE_GROUP').SPECIFIC.SETTINGS,
})
const mapDispatchToProps = (dispatch)=>{
  return {
    updateUserGroups: (groups)=>{dispatch(actions.userGroups(groups))},
    updateCurrentGroup: (group)=>{dispatch(actions.currentGroup(group))}
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GroupSettings)) 