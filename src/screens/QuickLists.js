import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { actions } from '../reducer'
//Local Modules
import db from '../lib/modules/database'
//Components
import Footer from './components/Footer'
import QuicklistsIndex from './subScreens/QuicklistsIndex'
import DesktopNav from './components/DesktopNav'

class QuickLists extends React.Component {
  _addNewList = (listData)=>{
    db.quick.createList(this.props.userInfo.email, listData ).then((list)=>{
      const newList = this.props.quicklists
      newList.push(list)
      this.props.updateQuicklists(newList)
    })
  }

  componentDidMount() {
    setTimeout(() => {
      if(this.props.screenTransition !== 'slideForward') {
        this.props.setScreenTransition("slideForward")    
      }
    }, 350);
  }

  render(){
    //const PAGE = PAGES.TRANSLATE(this.props.lang, 'QUICKLISTS')
    return(
      <div className="wrapper">
        <DesktopNav />    
        <QuicklistsIndex />
        <Footer page="QUICKLISTS" />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userInfo: state.userInfo,
  lang: state.language,
  quicklists: state.quicklists,
  screenTransition: state.screenTransition,
})

const mapDispatchToProps = (dispatch) => {
  return {
    updateQuicklists: (data)=>{ dispatch( actions.updateQuicklists(data) ) },
    setScreenTransition: (state)=>{dispatch(actions.setScreenTransition(state))}
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(QuickLists))
