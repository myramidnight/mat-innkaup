import en from '../lib/constants/translations/en'
import is from '../lib/constants/translations/is'

//List of translations
const translations = [
  {lang: "Icelandic", translation: is },
  {lang: "English", translation: en }
]
//Base language to compare to
const baseLang = {lang: "Icelandic", translation: is}

/* ---------------------------------------------------
  The Test
---------------------------------------------------- */
translations.forEach((language, i)=>{
  describe(`Comparing  ${language.lang} translation to base translation (${baseLang.lang})`, ()=>{
    //turn object keys into an array of key names
    const translationKeys = Object.keys(language.translation)
    const baseKeys = Object.keys(baseLang.translation)

    //first stage of test
    it(`Translation has the same pages as base language`,(done)=>{
      expect(translationKeys).toEqual(baseKeys)     
      done()
    })

    describe(`Each page in translation has the same properties as base`,()=>{
      //run test on each page if first stage passed
      
      //If property has object as value, need to test that as well
      translationKeys.forEach((page, i)=>{
        const currentPage = translationKeys[i]
        it(`${currentPage} has same properties as base`, ()=>{
          const pageKeys = Object.keys(language.translation[page])
          const basePageKeys = Object.keys(baseLang.translation[page])    
          
          expect(pageKeys).toEqual(basePageKeys)

          //trying to test if page has subpages
          /*pageKeys.forEach((subPage,i)=>{
            if(typeof subPage !== "string"){
            }
          })*/
          
        })
      })
    })
    

  })
})