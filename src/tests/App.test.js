import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';
import {  HashRouter as Router } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import reducer  from '../reducer'

//The redux storage
const store = createStore( reducer )

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <App />
      </Router>
    </Provider>
  , div);
  ReactDOM.unmountComponentAtNode(div);
});
