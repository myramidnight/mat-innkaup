//List of variables for screen locations
//use this instead of manually typing in the path for inside links
//this allows the path to be changed throughout the app
export default {
  ROOT : '/mat-innkaup',
  HOME : '/',
  GROUPS : '/groups',
  SINGLE_GROUP : '/groups/',
  QUICKLISTS : '/quicklists',
  SINGLE_QUICKLIST : '/quicklists/',
  LOGIN : '/login' ,
  SAVINGS: '/savings',
  ACCOUNTING: '/accounting',
  DELIVERY: '/delivery',
  USER_SETTINGS: '/usersettings',
  ERROR_404: '/404',
  GUIDE: '/guide',
  SINGLE_ACCOUNTING: '/accounting/'
}