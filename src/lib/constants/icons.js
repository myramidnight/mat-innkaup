/* 
  This file is a central module for easy access to all the SVG images,
  instead of having to import them individually like here below,
  this file will 
*/
import svg from '../images/svgPaths'
//imported images
import logo from '../images/logo.svg'
import checkbox from '../images/icons/checkbox.svg'
import checkboxChecked from '../images/icons/checkbox_checked.svg'
//imported icons
import back from '../images/icons/icon_backward.svg'
import check from '../images/icons/icon_check.svg'
import coin from '../images/icons/icon_coin.svg'
import delivery from '../images/icons/icon_delivery.svg'
import edit from '../images/icons/icon_edit.svg'
import exit from '../images/icons/icon_exit.svg'
import forward from '../images/icons/icon_forward.svg'
import list from '../images/icons/icon_list.svg'
import menu from '../images/icons/icon_menu.svg'
import piggy from '../images/icons/icon_piggy.svg'
import gear from '../images/icons/icon_gear.svg'
import add from '../images/icons/icon_add.svg'
import starFill from '../images/icons/icon_star_fill.svg'
import star from '../images/icons/icon_star.svg'
import trash from '../images/icons/icon_trash.svg'
import user from '../images/icons/icon_user.svg'
import userFill from '../images/icons/icon_user_fill.svg'
import toggle_grey from '../images/icons/toggle_grey.svg'
import toggle_orange from '../images/icons/toggle_orange.svg'

const icon = { 
  logo,
  //settings and navigation
  back,
  check,
  edit,
  exit,
  forward,
  menu,
  gear,
  add,
  star,
  starFill,
  trash,
  //valmynd
  coin,
  delivery,
  piggy,
  list,
  user,
  userFill,
  //input images
  checkbox,
  checkboxChecked,
  toggle_grey,
  toggle_orange,
}

export default {
  //the svg images for static use
  icon: icon,
  //svg data as an array of objects
  svg: svg,
}