//a 'constants' file for setting up the data for the database

const userInfo = (user, groups)=>{
  groups = groups || []
  return {
    email: user.email,
    displayName: user.displayName || user.email.slice(0,user.email.indexOf("@")),
  }
}
const changeLogEntry = ({email, data, change, type})=>{
  //Create the basic changelog entry
  return {
    change: change, //was it 'created', 'modified' or 'removed'
    type: type, //was it an 'item', 'group', 'member' or 'quicklist'?
    member: email, //who made the change
    data: data, //default object for either item or group
    time: Date.now(), //when did the change happen?
  }
}

const quickItem = (itemData)=>{
  //return the item
  return {
    title: itemData.title,
    quantity: itemData.quantity,
    added: Date.now()
  }
}

const newItem = (email,itemData)=>{
  //gets itemData from 'src/screens/components/CreateNewItem.js'
  const logEntry = changeLogEntry({email:email, type: 'item', data: itemData, change: 'created'})
  //return the item
  return {
    created: logEntry.time,
    title: itemData.title,
    quantity: itemData.quantity,
    latestChange: logEntry,
  }
}
const updateItem = (email,itemData)=>{
  const logEntry = changeLogEntry({email:email, data:itemData, type:'item' , change: 'modified' })
  //add the logEntry to the changelog
  itemData.changeLog.push(logEntry)
  //returns the logEntry with updated 'latestChange'
  return { 
    ...itemData,
    latestChange: logEntry
  }
}

const newGroup = (userInfo,groupData)=>{
  const logEntry = changeLogEntry({email:userInfo.email,data:groupData, type:'group', change: 'created'})
  return {
    created: logEntry.time,
    title: groupData.title,
    latestChange: logEntry,
    members: [userInfo.email]
  }
}
const updateGroup = (email,groupData)=>{
  const logEntry = changeLogEntry({email:email,data:groupData, type:'group', change: 'modified'})
  //add the logEntry to the activityLog
  groupData['activityLog'].push(logEntry)
  //returns the logEntry with updated 'latestChange'
  return {
    ...groupData,
    latestChange: logEntry 
  }
}

const reduxStateForStorage = (reduxState)=>{
  const keep = [ //what to keep in the object for localStorage
    "userGroups",
    "quicklists",
    "currentGroup",
    "language",
    "userInfo"
  ]
  return Object.keys(reduxState)
    .filter((key)=>(keep.includes(key))) //filter out the unwanted info
    .reduce((state, key)=>{ //copies the object to create a new one
      state[key] = reduxState[key]
      return state
    }, {})
}

export default {
  userInfo: userInfo,
  logEntry: changeLogEntry,
  newItem: newItem,
  quickItem,
  updateItem: updateItem,
  newGroup: newGroup,
  updateGroup: updateGroup,
  reduxObject: reduxStateForStorage,
}