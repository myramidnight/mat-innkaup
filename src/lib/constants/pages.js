/* 
This is a module that will handle translated text, 
The translations are a array of objects, each object containing 
the traslation for a single page. 

Each translation file should contain the same key properties,
else an error might occur when rendering translated content. 

You will find the translations in 'src/lib/constants/translations',
and they are named after the short code for each language

If a new language is added, then please add a new 'case' in the
translate function below and connected it to the imported translation
*/

//import page paths
import paths from './routerPaths'

//import translations from 'src/lib/translations' folder
import is from './translations/is'
import en from './translations/en'

const translate = (language)=>{
  //switch what language is returned
  switch (language.toLowerCase()) {
    case 'is' : return is
    case 'en' : return en
    //if no language matches, then default to english
    default   : return en
  }
}

const translatedPage = (language, page)=>{
  //returns the language object for specified page
  const lang = translate(language)
  return lang[page.toUpperCase()]
}

export default {
  TRANSLATE: translatedPage,
  PATHS: paths,
}

