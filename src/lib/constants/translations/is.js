/*
  the PAGE_NAMES are based on the names in '../routerPaths'

  Every object has a title, short title, description and short description 
  and page specific translations (PAGE_NAME.SPECIFIC)

  use the language module, instead of this file directly
*/
export default {
  INTERFACE: {
    LISTS: {
      TITLE: 'Gera nýjan lista',
      NAME: 'Nafn á lista',
      QUICKLIST: 'Gera nýjan flýtilista',
      GROUP: 'Gera nýjan innkaupalista',
      ADD: 'Bæta við lista',
      EDIT_LIST: 'Breyta lista',
      DELETE_LIST: 'Eyða lista',
    },
    PRODUCT: {
      TITLE: 'Bæta við nýrri vöru',
      NAME: 'Vöruheiti',
      QUANTITY: 'Magn',
      ADD: 'Bæta við listann',
      REMOVE_ITEM: 'Fjarlægja vöru',
    },
    MEMBERS: {
      SINGLE: 'meðlimur',
      PLURAL: 'meðlimir'
    },
    CONFIRM: {
      TITLE: 'Stillingar',
    }
  },
  //icelandic translation
  'ACCOUNTING': { //Bókhald
    TITLE: 'Bókhald',
    SHORT_TITLE: 'Bókhald',
    DESC: 'Veldu hópinn sem þú vilt skoða',
    SHORT_DESC: '',
    SPECIFIC: {},
  },
  'DELIVERY': { //Heimsendingar
    TITLE: 'Verslanir',
    SHORT_TITLE: 'Verslanir',
    DESC: 'Veldu verslun til að versla í' ,
    SHORT_DESC: '',
    SPECIFIC: {},
  },
  'GROUPS': { //Innkaupalistar - hópar
    TITLE: 'Innkaupalistar',
    SHORT_TITLE: 'Innkaupalistar',
    DESC: 'Hér finnur þú innkaupalistana þína',
    SHORT_DESC: '',
    SPECIFIC: {
    },
  },
  'HOME': { //Valmynd
    TITLE: 'Valmynd',
    SHORT_TITLE: 'Valmynd',
    DESC: 'Valmynd',
    SHORT_DESC: '',
    SPECIFIC: {
      NAV: {
        BACK: 'Til baka',
        HOME: 'Valmynd',
        FORWARD: 'Áfram'
      },
    },
  },
  'LOGIN': { //Innskráning
    TITLE: 'Innskráning',
    SHORT_TITLE: 'Innskráning',
    DESC: 'Þetta er Google Login, smelltu bara á innskrá',
    SHORT_DESC: 'Google Login',
    SPECIFIC: {
      //InputFields
      USERNAME: 'Netfang',
      PASS: 'Lykilorð',
      PASS_CONFIRM: 'Endurtaka lykilorð',
      //Submit
      SUBMIT_SIGNUP: 'Búa til aðgang',
      SUBMIT_SIGNIN: 'Skrá mig inn',
      //SwapToggle
      WELCOME: 'Velkomin aftur',
      SIGNIN: 'Innskráning',
      REGISTER: 'Búa til aðgang',
      //Google login
      OR: 'eða',
      GOOGLE: 'Google innskráning',
    },
  },
  'QUICKLISTS': { //Uppáhalds - flýtilistar
    TITLE: 'Flýtilistarnir þínir',
    SHORT_TITLE: 'Flýtilistar',
    DESC: 'Bættu þeim á innkaupalistann þinn',
    SHORT_DESC: '',
    SPECIFIC: {
    },
  },
  'SAVINGS': { //Sparnaður
    TITLE: 'Sparnaður',
    SHORT_TITLE: 'Sparnaður',
    DESC: 'Veldu hópinn sem þú vilt skoða',
    SHORT_DESC: '',
    SPECIFIC: {},
  },
  'SINGLE_GROUP': { //Stakur innkaupalisti
    TITLE: 'Stakur innkaupalisti',
    SHORT_TITLE: 'Stakur innkaupalisti',
    DESC: '',
    SHORT_DESC: '',
    SPECIFIC: {
      YOU: 'Þú',
      SETTINGS: {
        TITLE: 'Stillingar',
        MEMBERS: {
          MEMBERS_LIST: 'Meðlimir',
          ADD_MEMBERS: 'Bæta við meðlimum',
          ADMIN: 'Stjórnandi',
          MODERATOR: 'Meðstjórnandi',
          NORMAL: 'Venjulegur notandi',
        },
        ADD: 'Bæta við',
        //Delete
        DELETE_LIST: 'Eyða hóp',
        DELETE_ITEMS: 'Eyða öllum vörum',
        NEW_MEMBERS: 'Nýjir meðlimir',
        SAVE_QUICKLIST: 'Vista sem flýtilista'
      },
    },
  },
  'SINGLE_QUICKLIST': { //Stakur flýtilisti
    TITLE: 'Stakur flýtilisti',
    SHORT_TITLE: 'Stakur flýtilisti',
    DESC: '',
    SHORT_DESC: '',
    SPECIFIC: {
      SETTINGS:{
        DELETE_LIST: 'Eyða flýtilista',
        EDIT: 'Breyta lista',
        SAVE: 'Vista lista',
      }
    },
  },
  'USER_SETTINGS': { //Aðgangsstillingar
    TITLE: 'Aðgangurinn þinn',
    SHORT_TITLE: 'Stillingar',
    DESC: 'aðgangs stillingarnar þínar',
    SHORT_DESC: 'breyttu stillingum',
    SPECIFIC: {
      LANGUAGE: {
        TITLE: 'Breyta tungumáli',
        LANG: 'English'
      },
      LOCATION: {
        TITLE:'Staðsetning',
        TOGGLE_LOCATION: 'Kveikja á staðsetningu'
      },
      NOTICE: {
        TITLE: 'Tilkynningar',
        SAVINGS: 'Sparnaður',
        DELIVERY: 'Heimsendingar',
        GROUPS: 'Innkaupalistar',
      },
      USER: {
        TITLE: 'Notandi',
        DELETE: 'Eyða aðganginum',
        LOGOUT: 'Skrá mig út'
      }
    },
  },
  '404': { //Error page
    TITLE: '404: Síðan fannst ekki',
    SHORT_TITLE: '404',
    DESC: '',
    SHORT_DESC: '',
    SPECIFIC: {},
  },
}
