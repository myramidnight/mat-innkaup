/*
  the PAGE_NAMES are based on the names in '../routerPaths'

  Every object has a title, short title, description and short description 
  and page specific translations (PAGE_NAME.SPECIFIC)

  use the language module, instead of this file directly
*/
export default {
  INTERFACE: {
    LISTS: {
      TITLE: 'Create new list',
      NAME: 'Name of new list',
      QUICKLIST: 'Add new quicklist',
      GROUP: 'Add new shoppinglist',
      ADD: 'Add',
      EDIT_LIST: 'Edit list',
      DELETE_LIST: 'Delete list',
    },
    PRODUCT: {
      TITLE: 'Add item to list',
      NAME: 'Item name',
      QUANTITY: 'Quantity',
      ADD: 'Add to list',
      REMOVE_ITEM: 'Remove item',
    },
    MEMBERS: {
      SINGLE: 'member',
      PLURAL: 'members'
    },
    CONFIRM: {

    }
  },
  //english translation
  'ACCOUNTING': {
    TITLE: 'Accounting',
    SHORT_TITLE: 'Accounting',
    DESC: '',
    SHORT_DESC: '',
    SPECIFIC: {},
  },
  'DELIVERY': {
    TITLE: 'Home Delivery',
    SHORT_TITLE: 'Delivery',
    DESC: '',
    SHORT_DESC: '',
    SPECIFIC: {},
  },
  'GROUPS': {
    TITLE: 'Shoppinglists / Groups',
    SHORT_TITLE: 'Shoppinglists',
    DESC: 'Your shoppinglists',
    SHORT_DESC: '',
    SPECIFIC: {
    },
  },
  'HOME': {
    TITLE: 'Navigation',
    SHORT_TITLE: 'Navigation',
    DESC: '',
    SHORT_DESC: '',
    SPECIFIC: {
      NAV: { 
        BACK: 'Back',
        HOME: 'Home',
        FORWARD: 'Forward'
      },
    },
  },
  'LOGIN': {
    TITLE: 'Login / Signup',
    SHORT_TITLE: 'Login',
    DESC: '',
    SHORT_DESC: '',
    SPECIFIC: {
      //InputFields
      USERNAME: 'Email',
      PASS: 'Password',
      PASS_CONFIRM: 'Repeat password',
      //Submit
      SUBMIT_SIGNUP: 'Create account',
      SUBMIT_SIGNIN: 'Login',
      //SwapToggle
      WELCOME: 'Welcome back',
      SIGNIN: 'Signin',
      REGISTER: 'Sign up',
      //Google login
      OR: 'or',
      GOOGLE: 'Google SignIn',
    },
  },
  'QUICKLISTS': {
    TITLE: 'Quicklists',
    SHORT_TITLE: 'Quicklists',
    DESC: 'Add these to your shoppinglists',
    SHORT_DESC: '',
    SPECIFIC: {
    },
  },
  'SAVINGS': {
    TITLE: 'Savings',
    SHORT_TITLE: 'Savings',
    DESC: '',
    SHORT_DESC: '',
    SPECIFIC: {},
  },
  'SINGLE_GROUP': {
    TITLE: 'Single shoppinglist',
    SHORT_TITLE: 'Single list',
    DESC: '',
    SHORT_DESC: '',
    SPECIFIC: {
      YOU: 'You',
      SETTINGS: {
        TITLE: 'Settings',
        MEMBERS: {
          MEMBERS_LIST: 'Members',
          ADD_MEMBERS: 'Add new members',
          ADMIN: 'Admin',
          MODERATOR: 'Moderator',
          NORMAL: 'Regular member'
        },
        ADD: 'Add',
        DELETE_LIST: 'Delete list',
        DELETE_ITEMS: 'Delete all items',
        NEW_MEMBERS: 'New members',
        SAVE_QUICKLIST: 'Save as quicklist'
      },
    },
  },
  'SINGLE_QUICKLIST': {
    TITLE: 'Single quicklist',
    SHORT_TITLE: 'Single quicklist',
    DESC: '',
    SHORT_DESC: '',
    SPECIFIC: {
      SETTINGS:{
        DELETE_LIST: 'Delete quicklist',
        EDIT: 'Edit list',
        SAVE: 'Save list',
      }
    }
  },
  'USER_SETTINGS': {
    TITLE: 'Account settings',
    SHORT_TITLE: 'Settings',
    DESC: '',
    SHORT_DESC: '',
    SPECIFIC: {
      LANGUAGE: {
        TITLE: 'Change language',
        LANG: 'Íslenska'
      },
      LOCATION: {
        TITLE:'Location',
        TOGGLE_LOCATION: 'Turn on location'
      },
      NOTICE: {
        TITLE: 'Notifications',
        SAVINGS: 'Savings',
        DELIVERY: 'Home delivery',
        GROUPS: 'Shoppinglists',
      },
      USER: {
        TITLE: 'User',
        DELETE: 'Delete account',
        LOGOUT: 'Logout'
      }
    },
  },
  '404': { 
    TITLE: '404: Page not found',
    SHORT_TITLE: '404',
    DESC: '',
    SHORT_DESC: '',
    SPECIFIC: {},
  },
}
