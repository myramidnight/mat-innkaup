import db from './dbBase'

/* ---------------------------------------------------
  Fetch items from database
---------------------------------------------------- */
const fetchItemsFromDb = (group)=>{
  return db.getDocs('groups/' + group.id + '/items').then((items)=>{
    console.log("Items!",items)
    const itemsList = []
    //add the ID to the items in array
    items.forEach((item)=>{ 
      console.log("Item!", item.id)
      itemsList.push({ ...item.data(), id: item.id})  
    })
    //console.log("Finished fetching items for group '" + group.title + "' from database: ", itemsList)
    return itemsList
  }).catch((error)=>{
    console.log("ERROR in fetchItemsFromDb in database.js: ", error )
  })
}

/* ---------------------------------------------------
  Add or remove items from group lists
---------------------------------------------------- */
const addItemToGroupOnDb = (group, data)=>{
  const collection = 'groups/' + group.id + '/items'
  return db.addDoc(collection, data).then((newItem)=>{
    //add the id to the item data
    const item = { ...data, id: newItem.id}
    //console.log(item.title + " has been added to ''" + group.title + "' in database: ", item)
    return item
  }).catch((error)=>{
    console.log("ERROR in addItemToGroupOnDb in database.js: ", error )
  })
}

const removeItemFromDb = (group, item)=>{
  //delete the item from database
  return db.deleteDoc('groups/' + group.id + '/items', item.id).then(()=>{
    //console.log(item.title + " has been deleted from '" + group.title +  "' in database")
    return item
  }).catch((error)=>{
    console.log("ERROR in deleteItemFromGroupOnDb in database.js: ", error )
  })
}

/* ---------------------------------------------------
  exports
---------------------------------------------------- */
export default {
  fetchItems: fetchItemsFromDb, 
  addItem: addItemToGroupOnDb, 
  removeItem:removeItemFromDb, 
}