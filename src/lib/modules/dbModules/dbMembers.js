import db from '../database'

/* ---------------------------------------------------
  add member to group
---------------------------------------------------- */
const addMember = (email, newUser, groupData)=>{
  const randomBackground = ()=>{
    const color = Math.floor(Math.random()*16777215).toString(16);
    return("#"+color)
  }
  const subCollection = 'groups/'+ groupData.id + "/members"
  //first confirm with database that user exists
  return db.base.getDoc('users', newUser.email).then((user)=>{
    //if the user exists on the database, add the newUser to object
    if(user.exists){
      //add the time when member was added to group
      const userData = {
        ...newUser, //email and status
        displayName: user.data().displayName,
        added: Date.now(),
        color: randomBackground(),
      }
      //then create the new member in the members subcollection
      return db.base.setDoc(subCollection, newUser.email, userData).then((member)=>{
        //add the user email to members array and then return the userData
        return db.base.updateDoc('groups', groupData.id, {
          members: db.base.arrayUnion(newUser.email)
        }).then(()=>{ 
          //return the userData if sucessful
          return userData
        })
      })
     } else {
       //return false if user isn't registered
       return false
     }
  }).catch((error)=>{
    console.log("ERROR in addMemberToGroup:", error)
  })
}


/* ---------------------------------------------------
  remove members from group
---------------------------------------------------- */
const removeMembers = (user, members, groupData)=>{
  
  const myPromise = new Promise((resolve)=>{
    const membersCollection = 'groups/' + groupData.id + "/members"
    //return db.base.deleteDoc('groups/' + groupData.id + "/members", member.email)
    //create the changelog
    //remove member from database

    //How many members is being removed?
    if(members.length < 1){
      //removing single member
      db.base.deleteDoc(membersCollection, members[0].email)
      return resolve(true)
    } else {
      //removing multiple members
      //go through each member and remove them
      members.forEach((member)=>{ 
        db.base.deleteDoc(membersCollection, member.email)
        db.base.updateDoc('groups', groupData.id, {
          members: db.base.arrayRemove(member.email)
        })
      })
      return resolve(true)
    } 
  }).catch((error)=>{
    console.log("ERROR in remove (member) in removeMembersFromGroup:", error)
  })
  return myPromise
}

export default {
  addMember: addMember,
  removeMembers: removeMembers,
}