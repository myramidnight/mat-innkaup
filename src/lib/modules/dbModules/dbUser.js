import db from './dbBase'

const updateUserOnDb = ( userInfo)=>{
  //creates user on db with the email as id
  return db.setDoc('users', userInfo.email, userInfo).catch((error)=>{
    console.log("ERROR in updateUserOnDb in database.js: ", error )
  })
}

export default {
  updateUser: updateUserOnDb,
}