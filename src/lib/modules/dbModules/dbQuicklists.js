import db from '../database'

/* ---------------------------------------------------
  create quicklist
---------------------------------------------------- */
const createQuickList = (email, listData)=>{
  //when was the list created
  listData['created'] = Date.now()
  //then add it to the database
  return db.base.addDoc('users/' + email + "/quicklists", listData ).then((newList)=>{
    console.log("QuickList added to database")
    //return the listData with id
    return {
      ...listData,
      id: newList.id
    }
  }).catch((error)=>{
    console.log("ERROR in createQuickList: ", error )
  })
}
/* ---------------------------------------------------
  fetch existing quicklists
---------------------------------------------------- */
const fetchQuicklistItems = (collection)=>{
  return db.base.getDocs(collection).then((items)=>{
    const itemsList = []
    //add the ID to the items in array
    items.forEach((item)=>{ 
      itemsList.push({ ...item.data(), id: item.id})  
    })
    return itemsList
  }).catch((error)=>{
    console.log("ERROR in fetchQuicklistItems: ", error )
  })
}
const fetchQuickLists = (email)=>{
  const userQuicklists = 'users/'+email+"/quicklists"
  return db.base.getDocs(userQuicklists).then((lists)=>{
    const quickLists = []
    lists.forEach((list)=>{
      fetchQuicklistItems( userQuicklists+ "/" + list.id + "/items").then((listItems)=>{
        return quickLists.push({
          ...list.data(),
          id: list.id,
          items: listItems || []
        })
      })
    })
    return quickLists
  }).catch((error)=>{
    console.log("ERROR in fetchQuickLists: ", error )
  })
}
/* ---------------------------------------------------
  add items from quicklist
---------------------------------------------------- */
const addItemToQuicklist = (user, list, data)=>{
  const collection = 'users/' + user.email + '/quicklists/' + list.id + "/items"
  return db.base.addDoc(collection, data).then((newItem)=>{
    //add the id to the item data
    const item = { ...data, id: newItem.id}
    //console.log(item.title + " has been added to ''" + list.title + "' in database: ", item)
    return item
  }).catch((error)=>{
    console.log("ERROR in addItemToQuicklistOnDb: ", error )
  })
}

/* ---------------------------------------------------
  remove items from quicklist
---------------------------------------------------- */
const removeItemFromQuicklist = (user, list, item)=>{
  //delete the item from database
  return db.base.deleteDoc('users/' + user.email + '/quicklists/' + list.id + '/items', item.id).then(()=>{
    //console.log(item.title + " has been deleted from '" + group.title +  "' in database")
    return item
  }).catch((error)=>{
    console.log("ERROR in removeItemFromQuicklist: ", error )
  })
}

/* ---------------------------------------------------
  delete quicklist
---------------------------------------------------- */
const deleteQuicklist = (email, groupData)=>{
  return db.base.deleteDoc('users/' + email + "/quicklists", groupData.id)
}
/* ---------------------------------------------------
  add items from quicklist to group
---------------------------------------------------- */
const addItemsToGroup = (groupData, items)=>{
  const myPromise = new Promise((resolve)=>{
    const itemsList = items.map((item)=>{
      return db.items.addItem(groupData, item)
    })
    return resolve(itemsList)
  }).catch((error)=>{
    console.log("ERROR in addItemsToGroup: ", error )
  })
  return myPromise
}
/* ---------------------------------------------------
  exports
---------------------------------------------------- */
export default {
  createList: createQuickList,
  addItem: addItemToQuicklist,
  addToGroup: addItemsToGroup,
  removeItem: removeItemFromQuicklist,
  fetchLists: fetchQuickLists,
  deleteList: deleteQuicklist,
}