import db from '../database'

/* ---------------------------------------------------
  fetch groups from database
---------------------------------------------------- */
const fetchGroupsFromDb = (email)=>{
  return db.base.fetchMatch('groups', 'members', 'array-contains', email).then((groups)=>{
    
    const randomBackground = ()=>{
      const color = Math.floor(Math.random()*16777215).toString(16);
      return("#"+color)
    }
    const groupsList = []
    //create an array of groups with the data and id
    groups.forEach((doc)=>{   
      return db.base.getDocs('groups/'+ doc.id + "/members").then((members)=>{ 
        //Fetch the members for this group
        const membersList = []
        let yourAccess = ""
        //populate the list with members from subCollection 'members'
        members.forEach((member)=>{ 
          membersList.push( {
            ...member.data(), 
            color:randomBackground()
          })
            //extract the user's access status from the members list
            if(member.data().email === email){
              yourAccess = member.data().status
            }
        })
        //push the group object to array of groups
        const thisGroup = { 
          ...doc.data(), 
          id: doc.id, 
          access: membersList ,
          yourAccess: yourAccess,
        }
        //continue with the currentGroup object
        return thisGroup
      }).then((thisGroup)=>{
        //Fetch the items for this group
        return db.base.getDocs('groups/' + doc.id + '/items').then((items)=>{
          const itemsList = []
          items.forEach((item)=>{
            itemsList.push({...item.data(), id: item.id})
          })
          return itemsList
        }).then((items)=>{
          //add the list of items to currentGroup before pushing it to groupsList array
          thisGroup['items'] = items
          groupsList.push(thisGroup)
        })
      })
    })
    //return the promised list of groups with items and members
    return groupsList 
  }).catch((error)=>{
    console.log("ERROR in fetchGroupsFromDb: ", error )
  })
}

/* ---------------------------------------------------
  add new group to database
---------------------------------------------------- */
const addGroupToDb = ( user, groupData )=>{
  //create the new group
  return db.base.addDoc( 'groups' , groupData ).then((newGroup)=>{
    //create the admin user to add as member
    console.log("Group added to database")
    //create the groupInfo, and add the user as admin in access
    const groupInfo = {
      ...groupData,
      id: newGroup.id,
      yourAccess: 'admin',
      items: [],
    }
    //add the user as admin to members subcollection
    return db.members.addMember(user.email, {email: user.email, status:'admin', displayName: user.displayName}, groupInfo).then((newMember)=>{
      console.log("New member in "+ groupData.title, newMember)
      groupInfo['access'] = [newMember]
    }).then(()=>{
      return groupInfo
    })
  }).catch((error)=>{
    console.log("ERROR in addGroupToDb: ", error )
  })
}

/* ---------------------------------------------------
  delete group from database 
---------------------------------------------------- */
const deleteGroupFromDb = (groupData)=>{
  //should not delete group if it contains members (other than admin)
  return db.base.deleteDoc('groups', groupData.id)
}
/* ---------------------------------------------------
  exports
---------------------------------------------------- */
export default {
  addGroup: addGroupToDb, 
  fetchGroups: fetchGroupsFromDb, 
  //group settings
  deleteGroup: deleteGroupFromDb,
}