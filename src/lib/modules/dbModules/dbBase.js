import firebase from '../firebase'
const db = firebase.firestore()

const collection = (collection)=>{
  return db.collection(collection)
}

const addDoc = ( collection , data )=>{
  //create a new document in the collection
  return db.collection( collection ).add( data )
}

const setDoc = ( collection, id, data )=>{
  //create or overwergitite a document in a collection
  return db.collection( collection ).doc( id ).set( data )
}

const updateDoc = (collection, id, data)=>{
  //update an existing document
  return db.collection(collection).doc(id).update(data)
}

const deleteDoc = (collection, id)=>{
  //delete a document from the collection
  return db.collection(collection).doc(id).delete()
}
const getDoc = (collection, id)=>{
  return db.collection(collection).doc(id).get()
}
const getDocs = (collection)=>{
  //fetch all documents in the collection
  return db.collection(collection).get()
}

const fetchMatch = (collection, prop, condition, match)=>{
  //fetch all documents in collection that match the conditions
  return db.collection(collection).where(prop, condition, match).get()
} 

const getCollections = (collection, id)=>{
  return db.collection(collection).doc(id).getCollections()
}
export default {
  collection: collection,
  getCollections: getCollections,
  //CRUD documents
  addDoc: addDoc,
  setDoc: setDoc,
  updateDoc: updateDoc,
  deleteDoc: deleteDoc,
  //fetch documents
  getDocs: getDocs,
  getDoc: getDoc,
  fetchMatch: fetchMatch,
  //firestore 
  arrayUnion: firebase.arrayUnion,
  arrayRemove: firebase.arrayRemove
}