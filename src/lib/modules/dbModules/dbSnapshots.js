import db from './dbBase'

const dbItemsWatcher = (dbCollection, currentItemsList, callback)=>{
  currentItemsList = currentItemsList || []
  return db.collection(dbCollection).onSnapshot(querySnapshot => {
    //What kind of 'change' happened?
    querySnapshot.docChanges().forEach(change => {
      const itemData = change.doc.data()
      //add the item id for reference
      itemData['id'] = change.doc.id
      //check if item is already on currentLIst
      const itemOnCurrentList = ()=>{
        return currentItemsList.find((item)=>{
          //if the item is already on currentList
          if(item.id === itemData.id){
            return true
          } else { 
            return false
          }
        })
      }
      //If the change was adding an item
      if (change.type === 'added') {
        //if added item was not already on the list
        if(!itemOnCurrentList()){
          callback("added", itemData)
        }
      }
      //if the change was modifying an item
      if (change.type === 'modified') {
        callback("modified", itemData)
      }
      //if the change was removing an item
      if (change.type === 'removed') {
        callback("removed", itemData)
      }
    })
  })
}

const groupsWatcher = (user, currentGroups, callback)=>{
  currentGroups = currentGroups || []
  return db.collection("groups").where('members', 'array-contains', user).onSnapshot(querySnapshot => {
    querySnapshot.docChanges().forEach(change => {
      const groupData = change.doc.data()
      //add the item id for reference
      groupData['id'] = change.doc.id
      //check if item is already on currentLIst
      const itemOnCurrentList = ()=>{
        return currentGroups.find((group)=>{
          //if the item is already on currentList
          if(group.id === groupData.id){
            return true
          } else { 
            return false
          }
        })
      }
      //If the change was adding an item
      if (change.type === 'added') {
        //if added item was not already on the list
        if(!itemOnCurrentList()){
          callback("added", groupData)
        }
      }
      //if the change was modifying an item
      if (change.type === 'modified') {
        callback("modified", groupData)
      }
      //if the change was removing an item
      if (change.type === 'removed') {
        callback("removed", groupData)
      }
    })
  })
}

export default {
  itemsWatcher: dbItemsWatcher,
  groupsWatcher: groupsWatcher,
}