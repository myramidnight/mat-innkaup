/*
** 'collection' property should be written in a specific format:
** 'collection/document/collection/document... '
**  and it can go as deep as needed:
**
** https://firebase.google.com/docs/firestore/
**
*/

import dbBase from './dbModules/dbBase'
import dbGroups from './dbModules/dbGroups'
import dbUser from './dbModules/dbUser'
import dbItems from './dbModules/dbItems'
import dbObjects from '../constants/dbObjects'
import dbSnapshots from './dbModules/dbSnapshots'
import dbMembers from './dbModules/dbMembers'
import dbQuicklists from './dbModules/dbQuicklists'

export default {
  base: dbBase,
  collection: dbBase.collection,
  groups: dbGroups, 
  members: dbMembers,
  user: dbUser,
  items: dbItems, 
  snapshots: dbSnapshots, 
  dbObj: dbObjects, 
  quick: dbQuicklists,
}