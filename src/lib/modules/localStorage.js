import dbObject from '../constants/dbObjects'

const localStorage = window.localStorage
//list of localStorage variables
const STORAGE_VAR = {
  STATE : 'mat-innkaup::redux-state'
}

const setStorage = (variable, data)=>{
  //creates and updates localStorage
  localStorage.setItem(variable, JSON.stringify(data))
  //console.log("Created/Overwrote localStorage: ", variable)
}

export const getStorage = (variable)=>{
  //fetch localStorage
  const data = JSON.parse(localStorage.getItem(variable))
  //console.log("Fetched from localStorage: ", variable)
  return data
}

const removeStorage = (variable)=>{
  //removes localStorage variable
  localStorage.removeItem(variable)
  console.log("Removed from localStorage: " + variable)
}

export const clearStorage = ()=>{
  //clears the localStorage
  localStorage.clear()
  console.log("Cleared localStorage!")
}

/* CHECK IF VARIABLE EXISTS  */
const checkStorage = (variable)=>{
  if(localStorage[variable] != null){
    //return the data for this variable if it exists
    return true
  } else {
    //variable not found
    return false
  }
}

/* CREATE LOCALSTORAGE IF NONE  */
const createStorage = (variable, data)=>{
  const myPromise = new Promise((resolve)=>{
    //if the variable exists in the localStorage
    if (checkStorage(variable) !== false) {
      //then get the localStorage data
      resolve( getStorage(variable) ) 
    } else {
      //else put the data into localStorage
      setStorage(variable, dbObject.reduxObject(data))
    }
  }).catch((error)=>{
    console.log("ERROR in createStorage: ", error)
  })
  return myPromise
}

const updateStorageKey = (variable, key, value)=>{
  const myPromise = new Promise((resolve, reject)=>{
    //if the variable exists on localStorage
    if(checkStorage(variable) !== false ) {
      //fetch the current data from the localStorage
      const currentData = getStorage(variable)
      //if the key exists in the data
      if(key in currentData) { 
        //update the localStorage with data
        currentData[key] = value
        //put the data back in localStorage
        setStorage(variable, currentData)
        //then resolve the promise by returning the currentData
        resolve( currentData )
      } else {
        //the key does not match anything in existing data
        console.log('"' + key + '" is not a key within this localStorage data')
        reject( false ) 
      }
    }
  }).catch((error)=>{
    console.log("ERROR in updateStorageKey: ", error)
  })
  return myPromise
}

export const getLocalState = ()=>{
  const myPromise = new Promise((resolve)=>{
    //resolve promise by returning the stored data
    const localState = getStorage(STORAGE_VAR.STATE)
    resolve( localState ) 
  }).catch((error)=>{
    console.log("ERROR in getLocalState: ", error)
  })
  return myPromise
}

export const createLocalState = (data)=>{
  return createStorage(STORAGE_VAR.STATE, data)
}

export const updateLocalState = (state)=>{
  const myPromise = new Promise((resolve)=>{
    //overwrite the state in localStorage
    setStorage(STORAGE_VAR.STATE, dbObject.reduxObject(state))
    // resolve the promise by 
    resolve( true )
  }).catch((error)=>{
    console.log("ERROR in updateLocalState: ", error)
  })
  return myPromise
}

export const updateLocalStateKey = (key, data)=>{
  const myPromise = new Promise((resolve)=>{
    updateStorageKey(STORAGE_VAR, key, data)
  }).catch((error)=>{
    console.log("ERROR in updateLocalStateKey: ", error)
  })
  return myPromise
}

//Export the modules
export default {
  //basic CRUD localStorage actions
  'setStorage': setStorage,
  'getStorage': getStorage, //exported
  'removeStorage': removeStorage,
  'clearStorage' : clearStorage, //exported
  //Other localStorage actions
  'checkStorage' : checkStorage, 
  'createStorage' : createStorage, 
  'updateStorageKey' : updateStorageKey, 
  //functions specific to the redux state
  'createLocalState' : createLocalState,  //exported
  'getLocalState': getLocalState, //exported
  'updateLocalState': updateLocalState, //exported
  'updateLocalStateKey' : updateLocalStateKey, //exported
}