import firebase from 'firebase'
import { firebase as config } from '../constants/config'
// Initialize Firebase
firebase.initializeApp(config);

/* firebase firestore database */
const firestore = ()=>{
  return firebase.firestore()
}

/* firebase authentication service */
const auth = ()=>{
  return firebase.auth()
}

const loginGoogle = (method)=>{
  var provider = new firebase.auth.GoogleAuthProvider()
  if(method ==="popup"){
    //use popup login
    return firebase.auth().signInWithPopup(provider)
  } 
  if (method === "redirect") {
    //use redirect login
    return firebase.auth().signInWithRedirect(provider)
  }
}
const signInWithEmail = (email, password)=>{
  return firebase.auth().signInWithEmailAndPassword(email, password).catch((error)=>{
    console.log("ERROR in loginPassword: ", error.message )
  })
}
const createUserWithEmail = (email, password)=>{
  firebase.auth().createUserWithEmailAndPassword(email, password).catch((error)=>{
    console.log("ERROR in createUserWithEmail: ", error.message )
  })
}

const signOut = ()=>{
  return firebase.auth().signOut().then(()=>{
    console.log("User has been logged out...")
  })
}
const currentUser = ()=>{
  return firebase.auth().currentUser
}
const arrayUnion = (value)=>{
  return firebase.firestore.FieldValue.arrayUnion(value)
}

const arrayRemove = (value)=>{
  return firebase.firestore.FieldValue.arrayRemove(value)
}
//export the modules (somehow 'module.exports' doesn't work here)
export default {
  firestore: firestore,
  auth: auth,
  currentUser: currentUser,
  signOut: signOut,
  loginGoogle: loginGoogle,
  createUserWithEmail: createUserWithEmail,
  signInWithEmail: signInWithEmail,
  arrayUnion: arrayUnion,
  arrayRemove: arrayRemove,
}