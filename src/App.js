import React, { Component } from 'react'
import { Route, Switch, withRouter, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { actions } from './reducer'
//Constants
import PAGES from './lib/constants/pages'
import dbObj from './lib/constants/dbObjects'
//local modules
import { getLocalState, createLocalState, updateLocalStateKey } from './lib/modules/localStorage'
import db from './lib/modules/database'
import firebase from './lib/modules/firebase'
//Screens
import Login from './screens/Login'
import Groups from './screens/Groups'
import SingleGroup from './screens/SingleGroup'
import QuickLists from './screens/QuickLists'
import SingleQuickList from './screens/SingleQuickList'
import Accounting from './screens/Accounting'
import SingleAccounting from './screens/SingleAccounting'
import UserSettings from './screens/UserSettings'
import Savings from './screens/Savings'
import Delivery from './screens/Delivery'
import Guide from './screens/Guide'
import AnimFade from './screens/animations/AnimFade'


class Navigation extends Component {
  /* ---------------------------------------------------
    local components
  ---------------------------------------------------- */
  PrivateRoute = ({ component: Component, ...rest })=>{
    //PrivateRoute component
    return(
      <Route 
        {...rest}
        render={props=>
          //if the user is authenticated, render the component
	{
	  console.log("privateroute rendering");
	  console.log(this.props.isLoggedIn);
	
	  return this.props.isLoggedIn ? (
            <Component {...props} /> 
          ) : (
          //else redirect the user to login
            <Redirect 
              to={{
                //this cannot be directed to a privateRoute path
                pathname: PAGES.PATHS.LOGIN,
                state: { from: props.location }
              }} 
            />
          )
        }}
      />
    )
  }
  
  /* ---------------------------------------------------
    Component functions
  ---------------------------------------------------- */
  _updateReduxWithLocalStorage = ()=>{
    getLocalState().then(( state )=>{
      //IF LOCALSTORAGE DATA IS FOUND
      if( state !== null ) {
        //update the reduxState
        this.props.updateUserGroups( state.userGroups ) //get the userGroups
        this.props.updateLanguage( state.language) //get the language
        this.props.updatedCurrentGroup(state.currentGroup) //
      } else {
        //NO DATA FOUND IN LOCALSTORAGE
        createLocalState(this.props.reduxState )
      }
    }).then(()=>{
      /* ---------------------------------------------------
        Check authentication
      ---------------------------------------------------- */
      //CHECK THE USER AUTHENTICATION
      firebase.auth().onAuthStateChanged((user)=>{
        this.props.setLoading(false)
        if(user){
          //construct the userInfo object 
          const oUserInfo = dbObj.userInfo(user)
          //update the database with userInfo
          db.user.updateUser(oUserInfo).then(()=>{
            console.log("Successfully updated userInfo on database!")
          })
          //add the user info to redux state
          this.props.updateUserInfo(oUserInfo)
          //get the quicklists from the database
          
          db.quick.fetchLists(user.email).then((lists)=>{
            console.log("Quicklists", lists)
            this.props.updateQuicklists(lists)
            updateLocalStateKey('quicklists', lists)
          })
          //get the groups from the database
          db.groups.fetchGroups(user.email).then((groups)=>{
            //const databaseGroups = groups
            console.log("Groups from Database:", groups)
            groups.forEach((group)=>{
              console.log("Ooo, a group", group)
              /* ---------------------------------------------------
                NOTHING HAPPENS INSIDE
              ---------------------------------------------------- */
            })
            this.props.updateUserGroups(groups)
            updateLocalStateKey('userGroups', groups) //update userGroups on localStorage
            return groups
          })
          //update user status in redux state
          this.props.updatedLoginStatus(true)
        }
      })
    })
  }
  /* ---------------------------------------------------
    Component mount conditions
  ---------------------------------------------------- */
  componentDidMount(){
    this._updateReduxWithLocalStorage()
  }
  /* ---------------------------------------------------
    Component render
  ---------------------------------------------------- */
  render() {
    // The Switch will automatically swap to the correct component screens
    // depending on the path in the address, different components will render
    return (
        <AnimFade>
          <Switch location={this.props.location}>        
            {/* Public routes */}
            <Route exact path={PAGES.PATHS['LOGIN'] } component={Login}/>
            {this.props.isLoggedIn ? (null):( //Render the login page for any path if not logged in
              <Route component={Login}/>
            )}
            <Route exact path={PAGES.PATHS['HOME'] } component={Groups} />
            {/* Private routes (user will be redirected to login if not authenticated) */}
             <Route exact path={PAGES.PATHS['GUIDE'] } component={Guide}/>
            <Route exact path={PAGES.PATHS['GROUPS'] } component={Groups}/>
            <Route exact path={PAGES.PATHS['SINGLE_GROUP']  + ":groupKey"} component={SingleGroup}/>
            <Route exact path={PAGES.PATHS['QUICKLISTS'] } component={QuickLists}/>
            <Route exact path={PAGES.PATHS['SINGLE_QUICKLIST']  + ":id"} component={SingleQuickList}/>
            <Route exact path={PAGES.PATHS['ACCOUNTING'] } component={Accounting}/>
            <Route exact path={PAGES.PATHS['SINGLE_ACCOUNTING']  + ":id"} component={SingleAccounting}/>
            <Route exact path={PAGES.PATHS['SAVINGS'] } component={Savings}/>
            <Route exact path={PAGES.PATHS['DELIVERY'] } component={Delivery}/>
            <Route exact path={PAGES.PATHS['USER_SETTINGS'] } component={UserSettings}/>
            {/* any path that is not specified goes to the following screen */}
            
            <this.PrivateRoute component={Groups} /> 
          </Switch>
        </AnimFade>
    );
  }
}
/* ---------------------------------------------------
  Redux state and dispatch
---------------------------------------------------- */
const mapStateToProps = (state)=>({
  reduxState: state,
  isLoggedIn: state.isLoggedIn,
  language: state.language,
  userGroups: state.userGroups,
})
const mapDispatchToProps = (dispatch) => {
  return {
    //fetch the shopping lists and update the redux store 
    updateUserStatus: ( status )=>{ dispatch( actions.isLoggedIn( status ) )},
    setLoading: (truefalse) => { dispatch(actions.setLoading(truefalse))},
    updatedLoginStatus: (status)=>{dispatch(actions.isLoggedIn(status))},
    updatedCurrentGroup: (group)=>{dispatch(actions.currentGroup(group))},
    updateUserGroups: (groups)=>{dispatch(actions.userGroups(groups))},
    updateQuicklists: (lists)=>{dispatch(actions.updateQuicklists(lists))},
    updateUserInfo: (user)=>{dispatch(actions.updateUserInfo(user))},
    updateLanguage: (lang)=>{dispatch(actions.updateLang(lang))}
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navigation))
