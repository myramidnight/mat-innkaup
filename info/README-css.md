# CSS notes and reference
These notes talk about general CSS, but examples will be made using some SASS/SCSS (%extentions and nesting, for the most part). 
If you need a recap on SASS in this project, [then please read the SASS guidelines](/README-sass.md).

## Using EM and REM units
>>>
These are _relative unites_ that are calculated from **font-size**
While both units are based on font-size, they have a different base of reference:
* **EM** inherits it's size from the parent, 0.5em would be half the font-size of the parent.
* **REM** is calculated from the font-size of the `html` element, and will stay consistant
  * if you change the font-size of `html` element, the relative unites change with it.
  * if the `html` element has font-size of _16px_, then _1rem_ = 16px
  * 0.5rem would then be 8px, and 2rem = 32px

If a project is going by the 8bit grid (or any grid for that matter) then you can use the REM
unit as the grid reference, if that makes sense.

Using REM makes a project easy to scale, using some media query to alter the font-size of the `html` element depending on the situation and everything will scale accordingly. SASS lets you makes simple functions that can convert pixels to REM to save you the math ([an example has been made in the SASS guidelines](/README-sass.md#functions)).

```scss
html { font-size: 16 }; // = 1rem
```
>>>

## CSS selectors 
You should look over the [reference for CSS selectors](https://www.w3schools.com/cssref/css_selectors.asp), you can do great many things with them.
>>>
### Being specific with element type and classes
Specifying the type of element when you target it can serve as a reminder of what you're actually styling (makes it easier to know what element you're looking for), it also prevents styles to accidentally effect the wrong elements. 

A style can have different effects based on the type of element, even if they might share a class, if you specify the type.

```scss
%rounded-border {
  border-radius: 8px;
}

//this will style any element with the className 'rounded'
.rounded { @extend %rounded-border; }
//this will only style buttons with the className 'rounded'
button.rounded { @extend %rounded-border; }
//this will only style div elements with className 'rounded'
div.rounded { @extend %rounded-border;}
```

### Multiple classes on elements
You can apply multiple classes on an element by simply putting space between each class, and then you can target elements that specifically have both classes. This allows you to add deviations depending on the situation by applying the class. 

```html
<img class="thumbnail left" alt=""/>
```

```scss
//this will style any image with the 'thumbnail' class
img.thumbnail {
  height:100px;
  width:150px;
}
//this will style any image with the 'left' class
img.left {
  float: left;
}
//this will style any image with both 'thumbnail' and 'left' class
img.thumbnail.left {
  margin-right: 8px;
}
```
>>>