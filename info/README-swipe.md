Just keeping notes here about the `react-easy-swipe` module I found
* [React Easy Swipe (WIP)](http://react-easy-swipe.js.org/)

## Usage
```javascript
import Swipe from 'react-easy-swipe'

onSwipeStart(event) {
  console.log('Start swiping...', event);
}

onSwipeMove(position, event) {
  console.log(`Moved ${position.x} pixels horizontally`, event);
  console.log(`Moved ${position.y} pixels vertically`, event);
}

onSwipeEnd(event) {
  console.log('End swiping...', event);
}

<Swipe
  onSwipeStart={onSwipeStart}
  onSwipeMove={onSwipeMove}
  onSwipeEnd={onSwipeEnd}
>
  <h1>Touch Me Please ~~</h1>
</Swipe>
```

## Properties
>>>
  tagName: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object,
  children: PropTypes.node,
  onSwipeUp: PropTypes.func,
  onSwipeDown: PropTypes.func,
  onSwipeLeft: PropTypes.func,
  onSwipeRight: PropTypes.func,
  onSwipeStart: PropTypes.func,
  onSwipeMove: PropTypes.func,
  onSwipeEnd: PropTypes.func
>>>