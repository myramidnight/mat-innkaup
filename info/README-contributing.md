# Contributing to the project
Begin with reading the [information about starting the project](../README.md). Then you can return here for notes about contributing to this specific project
>>>
**Navigation:**
1. [Comment your Code](#comment-your-code)
2. [Name structure](#name-structure)
2. [Folder structure](#folder-structure)
3. [Javascript guidelines](#javascript-guidelines-code-standards)
    * [Variables](#variables)
    * [JS automatic semicolon insertion](#javascript-automatic-semicolon-insertion)
4. [React specific guidelines](#react-specific-guidelines)
    * [component names](#component-names)
    * [react functions](#react-functions)
    * [redux state](#using-the-redux-state)
5. [Using images in this project](#using-images-in-this-react-project)
6. [SASS guidelines](#styling-with-sass)
7. [Git notes](#using-git)


>>>

## Comment your code
Don't be shy about explaining what you are doing within the code.
**It helps others to understand your code quicker** without having to desipher the code. "Don't make me think..."

``` js
// single-line comment, works in javascript and scss 
It only comments out // everything past the '//' marks

/*  
    Multi-line comment, turns all text between them into a comment.
    works in javascript, scss and css
*/
```

```html
<!-- comment in HTML, works as multi-line comment -->
```

## Name structures
Just a quick overview of the name structures to stay consistant. There are more details about the javascript variable names below.
>>>
* 

>>>

## Folder structure

"You are here." Just basic folder map so you can find what you need.

>>>
* `/info` = readme files with notes/guidelines
* `/public` = the base DOM files, the `src` folder cannot use files from the `public`
    * `/images` = app icons for the manifest
* `/src` = **where we will be working**
    * `/lib` = asset library
        * `/images` = where we'll put our image files for the project 
        * `/constants` = something like global variables throught the app 
            *`language` = array with objects holding the translated strings of each page 
        * `/css` = where the `scss` files compile _( **do not add code here** , do it in the SCSS folder )_
        * `/modules` = local modules, such as localStorage functions and so on...
    * `/screens` = the pages/screens
        * `/components` = the react component parts
        * `/subScreens` = screens that are used in a toggle modal (example would be group settings, or login/register)
    * `/scss` = **where the styling happens**
        * `/partials` = where we'll keep our _partials (sections of style code)
            * `/components` = partials tied to specific components / screens
    * `/tests` = the CI tests for the project
>>>

## JavaScript guidelines (Code Standards)
Guidelines to help keep the code consistant. 
Even though everything we'll do will technically be `javascript`, 
we'll tall about general javascript and then react specifically (because it's picky)
>>>
### Variables
* You should really use `const` (constant variable, unchangable) or `let` (variable that lets you change it) instead of plain old `var`
* **variables should start with a small letter**
* separate words with a Capital letter: `thisIsAnExample`
* no `spaces` , `-` or `_` in variable names 
* name of a **function** should explain it's purpose
* the first letter should indicate the type of value (_with the exception of functions_): `string`, `integer`, `array` or `object`. 
   * This helps you understand the variable even without knowing it's value before
```js
    var sText = "string"    // string values
    var iNumber = 123       // integer values (numbers)
    var aList = [ ]         // array
    var oItem = { }         // object
    var myFunction = ()=>{} // functions are an exception

    /* indicating the value type is useful when using the variable */
    /* but names of functions need to describe their purpose */
```

### JavaScript Automatic Semicolon Insertion

Maybe you have noticed that in javascript coding, sometimes lines end with a semicolon `;` and at other times they are missing. 
This is because semicolons `;` are purely optional as a result of the _JavaScript Automatic Semicolon Insertion_. The code adds them if they are missing.
   * just don't start lines with parentheses `(`, it might concatinate to the nearest word infront of it (creating a unexpected function in the process)

>>>

## React specific guidelines
>>>

### component names
* The exeption to the rule in general javascript variables being _lower case_ is the **names of components**
    * the name of the exported component within the file should start with a `Capital` letter
    * The `filename` should also mirror the component that it is exporting, `MyComponent.js` would be exporting the `MyComponent` react component.

### react functions
* please start names of react functions with a underscore `_`, it helps indicate that it's specific to the react component 
* you do not need to use `const` when creating a function within react components
* please place your functions before the `render()` and any `componentWillMount()` or such react specific tasks.
* to use these functions, you call them with  `this._myReactFunction()`

```javascript
_myReactFunction = ()=>{
    //whatever the function should do 
}
```

### Using the Redux state
* think of redux as a global state, instead of using the local state within components
* You need to export the component with `connect()` to give the component access the redux state 
    * `mapStateToProps` is for fetching the redux state
    * `mapDispatchToProps` is for updating the redux state
    * the first parameter in `connect()` is reserved for `mapStateToProps` ( give it `null` if you only wish to update the redux state without fetching it)
* Since everything will be _mapped to props_, you will need to call the redux state with `this.props`
* [**Read more about React Redux connect()**](https://react-redux.js.org/using-react-redux/connect-mapstate)

>>>
## Using images in this React Project
React seems to be a bit esentric when it comes using images, images need to be imported to be usable.
* [**Read more about adding images in react**](https://facebook.github.io/create-react-app/docs/adding-images-fonts-and-files)

>>>
### The Icons file
We have created a single file that you can import to get access to all the SVG icons without having to manually import them individually.
* the file is a central HUB/module to access all the SVG files in a simple way within JavaScript / React
* `icons.icon.<iconName>` will let you access the static images to use with `<img>` tags and such.
* `icons.svg.<iconName>` gives you an object containing the SVG data and details, for use in the `SVG component` mentioned below.
    * this object might evolve, but if you need reference then simply view the `src/images/icons/svgPaths.js` for details

```javascript
import icons from '../lib/constants/icons'

<img src={icons.icon.trash} alt="" /> //outputs a static trash svg icon 
```
* it is located in `src/lib/constants/icons.js` and you should view it to see the names of each icon.
* Using the icons in `<img>` tags will be static, like a png or other images.

### SVG component
We have created a component, that lets you insert `SVG` images in the project, this lets you control the fill color among other things. 
Since vector images are actually a string of numbers and letters so that when it renders, it stays crisp and clean at any size. 
* This SVG component takes in a object that has that string and other details.
* It does not contain width and height, so they will appear as big as possible by default.
* You need to give it the `svg` data in the `data` property
* Optional props include `className`, `fill`, `height` and `width`
* Styling a parent element targeting the SVG will overwrite any custom fill color defined in the component props.
* If you add custom height/width, it will still keep it's proportions

In this example, we'll have the following stylesheet attached
```css
.violet svg {fill: violet}
```
```html
import icons from '../lib/constants/icons'
import SVG from './components/SVG'

<SVG data={icons.svg.trash}/> //will show a big trash can icon
<SVG data={icons.svg.trash} fill="green"/> //icon will be green
<div className="violet">
    <SVG data={icons.svg.trash} fill="orange"/> //icon will be big and violet
    <SVG data={icons.svg.trash} fill="green" height="30" width="30"/> //icon will be small and violet
    <SVG data={icons.svg.trash} fill="pink"/> //icon will be big and violet
</div>
```

>>>

## Styling with SASS
We'll be using SASS for managing the CSS in this project. SASS understands normal CSS in it's files if they have the `.scss` file type.
>>>
* Work within the `src/scss` folder (the `src/lib/css` folder is exlusivly for compiled _SCSS_ files)
* Pleace break the style into `_partials` when possible (for example, one partial for each screen or component is perfect) and `@import` them in the `styles.scss`
* **Remember to turn on the _sass watcher_ to automatically compile your code**, `sass --watch <scss-folder>:<css-folder>`
    * it needs it's own terminal because it stays running until you tell it to stop or close the terminal. 
* if your style isn't updating, then check on your _SASS watcher_, either it isn't running, is compiling into wrong folder or failed to compile due to error in code
```
sass --watch src/scss:src/lib/css
```
 [Read more of our SASS guidelines/notes](README-sass.md)

### Why are we using SASS/CSS?

* You can write normal CSS in `.scss` files
* It allows for nesting (which is really nice once you try it out)
* Lets you split up your code/style into many managable files, while it still all compiles into one single file.
* Having the code in managable files, it is easier to debug it.
* The compiled CSS file will look neat and professional, can even be minified.
* You shouldn't be asking why... instead why not? it makes your life easier in the end.

>>>

## Using Git
>>>
We have a file full of useful notes about using `git`, please look it over.
* [Read our Git info](README-git.md)

>>>